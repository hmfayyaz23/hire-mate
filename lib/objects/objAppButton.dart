import 'package:flutter/material.dart';

import '../global.dart';

class AppButton extends StatelessWidget {
  final String title;
  final Function onPressed;
  final bool isReverse;
  const AppButton({
    @required this.title,
    @required this.onPressed,
    this.isReverse = false,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      margin: EdgeInsets.only(top: 10),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
            side: BorderSide(color: GLOBAL_THEME_COLOR)),
        onPressed: onPressed,
        color: isReverse ? Colors.white : GLOBAL_THEME_COLOR,
        textColor: isReverse ? GLOBAL_THEME_COLOR : Colors.white,
        child: Text(title,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            )),
      ),
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-- Next page button
//-----------------------------------------------
class NextPageButton extends StatelessWidget {
  final String title;
  final IconData icon;
  final String count;
  final Color color;
  final Function onTap;

  NextPageButton({
    @required this.title,
    @required this.icon,
    this.count = '',
    this.color = Colors.black,
    Key key,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      leading: CircleAvatar(
        backgroundColor: GLOBAL_THEME_COLOR,
        foregroundColor: Colors.white,
        child: Icon(icon),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title,
            style: TextStyle(
              fontSize: 18,
              color: color,
            ),
          ),

          Text(count,
            style: TextStyle(
              fontSize: 18,
              color: GLOBAL_THEME_COLOR,
            ),
          ),
        ],
      ),
      trailing:
      Icon(Icons.navigate_next, color: GLOBAL_THEME_COLOR),


    );
  }
}