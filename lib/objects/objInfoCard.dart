import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';

import '../global.dart';
import '../libWebData.dart';
import 'objTextDateFormat.dart';
import 'objCacheImage.dart';

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-- Information card for task list
//-----------------------------------------------
class InfoCard extends StatelessWidget {
  final dynamic imgLink;
  final String title;
  final String price;
  final String location;
  final String startDate;

  final String subtitle;
  final Function onTap;

  const InfoCard({
    Key key,
    @required this.imgLink,
    @required this.onTap,
    this.title = '',
    this.price = '',
    this.location = '',
    this.startDate = '',
    this.subtitle = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(10),
          width: double.infinity,
          height: 120,
          child: Row(
            children: <Widget>[
              //-----------------------
              //-- Image --
              //-----------------------
              CacheImage(
                imageURL: imgLink,
                width: 100*4/3,
                height: 100,
                iconSizeDummy: 60,
              ),
              SizedBox(width: 10),
              //-----------------------
              //-- Contents --
              //-----------------------
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //-- Title, Price and Description --
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          //-- Title --
                          Text(
                            title,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          //-- Price --
                          Text(
                            (price == '\$null') ? 'Negotiate' : price,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: GLOBAL_THEME_COLOR,
                            ),
                          ),
                        ],
                      ),
                      //-- Location --
                      Row(
                        children: <Widget>[
                          Icon(Icons.location_on,
                          size: 14,
                            color: GLOBAL_THEME_COLOR,
                          ),
                          SizedBox(width: 3),
                          Text(
                            location,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Icon(Icons.calendar_today,
                          size: 14,
                            color: GLOBAL_THEME_COLOR,
                          ),
                          SizedBox(width: 3),
                          Text(
                            startDate,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        subtitle,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: TextStyle(
                          fontSize: 12,
                          color: GLOBAL_THEME_COLOR,
                        ),
                      ),
                      //-- Status --
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-- Information card for quotation list
//-----------------------------------------------
class InfoCardQuote extends StatelessWidget {
  final dynamic imgLink;
  final String name;
  final String price;
  final String description;
  final Function onTap;

  InfoCardQuote({
    Key key,
    @required this.imgLink,
    this.name = '',
    this.price = '',
    this.description = '',
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(10),
          width: double.infinity,
          height: 70,
          //color: Colors.transparent,
          child: Row(
            children: <Widget>[
              //-----------------------
              //-- Image --
              //-----------------------
              Container(
                width: 50.0,
                height: 50.0,
                child: !(imgLink is String)
                    //-- No Image Link --
                    ? IconPlaceHolder(icon: Icons.person, iconSize: 40)
                    //-- Image link available --
                    : CachedNetworkImage(
                        imageUrl: imgLink,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            color: Colors.black12,
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                        placeholder: (context, url) => IconPlaceHolder(
                            icon: Icons.hourglass_empty, iconSize: 20),
                        errorWidget: (context, url, error) =>
                            IconPlaceHolder(icon: Icons.person, iconSize: 40),
                      ),
              ),
              SizedBox(width: 10),
              //-----------------------
              //-- Contents --
              //-----------------------
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        //-- Name --
                        Text(
                          name,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        //-- Price --
                        Text(
                          price,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: GLOBAL_THEME_COLOR,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 8),
                    //-- Description --
                    Text(
                      description,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-- Information card for Accepted list
//-----------------------------------------------
class InfoCardAccepted extends StatelessWidget {
  final QuoteData quoteData;
  final Function onTap;
  final Function onTapPhone;
  final Function onTapEmail;

  const InfoCardAccepted({
    Key key,
    @required this.quoteData,
    @required this.onTap,
    @required this.onTapPhone,
    @required this.onTapEmail,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(15),
          width: double.infinity,
          //height: 120,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //-----------------------
              //-- heading --
              //-----------------------
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      //-- seller picture --
                      CacheAvatar(
                        imageURL: quoteData.seller.picture,
                      ),
                      SizedBox(width: 10),
                      //-- seller name --
                      Text(
                        quoteData.seller.username,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      CircleButton(
                        iconData: Icons.phone,
                        onTap: onTapPhone,
                      ),
                      SizedBox(width: 10),
                      CircleButton(
                        iconData: Icons.email,
                        onTap: onTapEmail,
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10),
              //---------------
              //-- Job Image --
              //---------------
              CacheImage(
                width: MediaQuery.of(context).size.width - 30,
                height: (MediaQuery.of(context).size.width - 30)* 0.7,
                imageURL: quoteData.picture,
              ),
              SizedBox(height: 10),
              //---------------------------
              //-- title, date and price --
              //---------------------------
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //-- title --
                      Text(
                        quoteData.task.title,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      //-- date --
                      TextDateFormat(
                        dateString: quoteData.task.date_start,
                      )
                    ],
                  ),
                  //-- Price --
                  Text(
                    '\$${quoteData.quote_price}',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: GLOBAL_THEME_COLOR,
                    ),
                  ),
                ],
              ),
              Divider(color: Colors.grey),
              //-- Description --
              Text(
                quoteData.seller_description,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black54,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-- Information card for Notification list
//-----------------------------------------------
class InfoCardNotifications extends StatelessWidget {
  final dynamic imgLink;
  final String name;
  final String status;
  final String timeLog;
  final Function onTap;

  InfoCardNotifications({
    Key key,
    @required this.imgLink,
    @required this.onTap,
    this.name = '',
    this.status = '',
    this.timeLog = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(10),
          width: double.infinity,
          height: 75,
          //color: Colors.transparent,
          child: Row(
            children: <Widget>[
              //-----------------------
              //-- Image --
              //-----------------------
              CacheAvatar(
                imageURL: imgLink,
              ),
              SizedBox(width: 10),
              //-----------------------
              //-- Contents --
              //-----------------------
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        //-- Name --
                        Text(
                          name,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        //-- Price --
                        Text(
                          timeLog,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                    //-- Description --
                    Text(
                      status,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black54,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-- Circular button for phone and email
//-----------------------------------------------
class CircleButton extends StatelessWidget {
  final Function onTap;
  final IconData iconData;

  const CircleButton({Key key, this.onTap, this.iconData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 40.0;

    return InkResponse(
      onTap: onTap,
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: GLOBAL_THEME_COLOR,
          ),
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: Icon(
          iconData,
          color: GLOBAL_THEME_COLOR,
          size: 30,
        ),
      ),
    );
  }
}
