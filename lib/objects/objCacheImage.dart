import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../global.dart';

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-- Cache Image for avatar
//-----------------------------------------------
class CacheAvatar extends StatelessWidget {
  const CacheAvatar(
      {Key key,
      @required this.imageURL,
      this.radius = 25,
      this.iconSizeLoading = 20,
      this.iconSizeDummy = 40})
      : super(key: key);

  final dynamic imageURL;
  final double radius;
  final double iconSizeLoading;
  final double iconSizeDummy;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: radius * 2,
      height: radius * 2,
      child: !(imageURL is String)
          //-- No Image Link --
          ? IconPlaceHolder(icon: Icons.insert_photo, iconSize: iconSizeDummy, isCircular: true)
          //-- Image link available --
          : CachedNetworkImage(
              imageUrl: imageURL,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  color: Colors.black12,
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              placeholder: (context, url) => IconPlaceHolder(
                  icon: Icons.hourglass_empty, iconSize: iconSizeLoading, isCircular: true),
              errorWidget: (context, url, error) =>
                  IconPlaceHolder(icon: Icons.broken_image, iconSize: iconSizeDummy, isCircular: true),
            ),
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-- Cache image
//-----------------------------------------------
class CacheImage extends StatelessWidget {
  const CacheImage({
    Key key,
    @required this.imageURL,
    this.height = 200.0,
    this.width = double.infinity,
    this.iconSizeLoading = 50.0,
    this.iconSizeDummy = 100.0,
  }) : super(key: key);

  final dynamic imageURL;
  final double width;
  final double height;
  final double iconSizeLoading;
  final double iconSizeDummy;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: !(imageURL is String)
          //-- No Image Link --
          ? IconPlaceHolder(icon: Icons.image, iconSize: iconSizeDummy)
          //-- Image link available --
          : CachedNetworkImage(
              imageUrl: imageURL,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              placeholder: (context, url) => IconPlaceHolder(
                  icon: Icons.hourglass_empty, iconSize: iconSizeLoading),
              errorWidget: (context, url, error) =>
                  IconPlaceHolder(icon: Icons.broken_image, iconSize: iconSizeDummy),
            ),
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-- Icon placeholder
//-----------------------------------------------
class IconPlaceHolder extends StatelessWidget {
  final IconData icon;
  final double iconSize;
  final bool isCircular;
  const IconPlaceHolder(
      {Key key,
      @required this.icon,
      this.iconSize = 50.0,
      this.isCircular = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: isCircular
          ? BoxDecoration(
              color: Colors.black12,
              shape: BoxShape.circle,
            )
          : BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
      child: Icon(icon, color: GLOBAL_THEME_COLOR.withAlpha(128), size: iconSize),
    );
  }
}
