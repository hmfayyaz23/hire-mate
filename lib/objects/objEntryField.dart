import 'package:flutter/material.dart';

import '../global.dart';

class EntryField extends StatelessWidget {
  final Function onChanged;
  final Function onSubmitted;
  final Function onTap;
  final String label;
  final String prefix;
  final bool isObscureText;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final FocusNode focusNode;
  final int maxLines;
  final Color textColor;
  final bool readOnly;
  final bool isDense;

  const EntryField({
    Key key,
    this.onChanged,
    this.onSubmitted,
    this.onTap,
    this.label,
    this.prefix = '',
    this.isObscureText = false,
    this.controller,
    this.keyboardType,
    this.textInputAction,
    this.focusNode,
    this.maxLines = 1,
    this.textColor = Colors.black87,
    this.readOnly = false,
    this.isDense = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //margin: EdgeInsets.only(left: 30, right: 30, top: 10),
      child: TextField(
        onChanged: onChanged,
        onSubmitted: onSubmitted,
        onTap: onTap,
        obscureText: isObscureText,
        controller: controller,
        keyboardType: keyboardType,
        textInputAction: textInputAction,
        maxLines: maxLines,
        focusNode: focusNode,
        readOnly: readOnly,
        style: TextStyle(
          fontSize: 18,
          color: textColor,
        ),
        decoration: InputDecoration(
          isDense: ((label == null) || isDense),
          prefix: Text(prefix),
          labelText: label,
          labelStyle: TextStyle(
            fontSize: 20.0,
            color: Colors.grey
          ),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: GLOBAL_THEME_COLOR,
                width: 1,
              )),
        ),
      ),
    );
  }
}
