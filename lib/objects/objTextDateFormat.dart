import 'package:flutter/material.dart';

import 'package:intl/intl.dart';

import '../global.dart';

String getFormattedString(String dataString) {
  return DateFormat('EEE, MMM d, 20yy')
      .format(DateFormat('dd/MM/yy').parse(dataString));
}

String getTimeStampString(int timeStamp) {
  print(timeStamp);
  return DateFormat('dd, MMM HH:mm')
      .format(DateTime.fromMillisecondsSinceEpoch(timeStamp*1000).toUtc());
}

class TextDateFormat extends StatelessWidget {
  const TextDateFormat({Key key, @required this.dateString}) : super(key: key);

  final String dateString;

  @override
  Widget build(BuildContext context) {
    return Text(getFormattedString(dateString),
      style: TextStyle(
        fontSize: 14,
        color: GLOBAL_THEME_COLOR,
      ),
    );
  }
}


