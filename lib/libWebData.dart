import 'dart:io';

//---------------------------------------------------------
//---------------------------------------------------------
// Service location Data
//---------------------------------------------------------
class ServiceLocation {
  double lat;
  double lon;
  String addr;
  double distance;

  ServiceLocation({this.lat, this.lon, this.addr, this.distance});

  void printContent({String indent = ''}) {
    print('${indent}lat: $lat');
    print('${indent}lon: $lon');
    print('${indent}addr: $addr');
    print('${indent}distance: $distance');
  }

  factory ServiceLocation.cloneFrom(ServiceLocation srcData) {
    return ServiceLocation(
      lat: srcData.lat,
      lon: srcData.lon,
      addr: srcData.addr,
      distance: srcData.distance,
    );
  }

  factory ServiceLocation.fromJson(Map<String, dynamic> json) {
    return ServiceLocation(
      lat: double.parse(json['lat']),
      lon: double.parse(json['lon']),
      addr: json['addr'],
      distance: double.parse(json['distance']),
    );
  }

  Map<String, dynamic> getMap() => {
        'addr': addr,
        'lat': lat,
        'lon': lon,
        'distance': distance,
      };
}

//---------------------------------------------------------
//---------------------------------------------------------
// User Data
//---------------------------------------------------------
class UserData {
  double location_lat;
  double location_lon;
  dynamic picture;
  int id;
  String email;
  String username;
  String location;
  String phone;
  List<String> roles;
  //-- contractor only
  String service_type;
  List<String> category;
  List<ServiceLocation> service_locations;
  String abn;
  String business_type;
  String business_trading_name;

  UserData({
    this.location_lat,
    this.location_lon,
    this.picture,
    this.id,
    this.email,
    this.username,
    this.location,
    this.phone,
    this.roles,
    //-- contractor only
    this.service_type,
    this.category,
    this.service_locations,
    this.abn,
    this.business_type,
    this.business_trading_name,
  });

  bool isSeller() => roles.contains('contractor');

  void printContent({String indent = ''}) {
    print('${indent}id: $id');
    print('${indent}username: $username');
    print('${indent}email: $email');
    print('${indent}phone: $phone');
    print('${indent}location: $location');
    print('${indent}location_lat: $location_lat');
    print('${indent}location_lon: $location_lon');
    print('${indent}picture: $picture');
    print('${indent}roles:\n');
    roles.forEach((element) {
      print('$indent\t\t$element\n');
    });
    // contractor only
    if (isSeller()) {
      print('${indent}abn: $abn');
      print('${indent}business_type: $business_type');
      print('${indent}business_trading_name: $business_trading_name');
      print('${indent}service_type: $service_type');
      print('${indent}category:\n');
      category.forEach((element) {
        print('$indent\t\t$element\n');
      });
      print('${indent}service_locations:\n');
      service_locations.forEach((element) {
        element.printContent(indent: '$indent\t\t');
        print('\n');
      });
    }
  }

  factory UserData.cloneFrom(UserData srcData) {
    List<String> listRoles = [];
    srcData.roles.forEach((element) {
      listRoles.add(element);
    });
    // contractor
    if (srcData.isSeller()) {
      List<String> listCategory = [];
      srcData.category.forEach((element) {
        listCategory.add(element);
      });
      List<ServiceLocation> listServiceLocations = [];
      srcData.service_locations.forEach((element) {
        listServiceLocations.add(ServiceLocation.cloneFrom(element));
      });
      return UserData(
        location_lat: srcData.location_lat,
        location_lon: srcData.location_lon,
        picture: srcData.picture,
        id: srcData.id,
        email: srcData.email,
        username: srcData.username,
        location: srcData.location,
        phone: srcData.phone,
        roles: listRoles,
        //contractor only
        abn: srcData.abn,
        business_type: srcData.business_type,
        business_trading_name: srcData.business_trading_name,
        service_type: srcData.service_type,
        category: listCategory,
        service_locations: listServiceLocations,
      );
      // customer
    } else {
      return UserData(
        location_lat: srcData.location_lat,
        location_lon: srcData.location_lon,
        picture: srcData.picture,
        id: srcData.id,
        email: srcData.email,
        username: srcData.username,
        location: srcData.location,
        phone: srcData.phone,
        roles: listRoles,
      );
    }
  }

  factory UserData.fromJson(Map<String, dynamic> json) {
    //id could be int or string
    var idData = (json['id'] is int) ? json['id'] : int.parse(json['id']);

    List listData;
    List<String> rolesList;
    try {
      listData = json['roles'] as List<dynamic>;
      rolesList = listData.map((element) => element.toString()).toList();
    } catch (e) {
      rolesList = ['customer'];
    }

    //-- Customer type --
    if (!rolesList.contains('contractor')) {
      return UserData(
        location_lat: double.parse(json['location_lat']),
        location_lon: double.parse(json['location_lon']),
        picture: json['picture'],
        id: idData,
        email: json['email'],
        username: json['username'],
        location: json['location'],
        phone: json['phone'],
        roles: rolesList,
      );
      //-- Contractor type --
    } else {
      List<String> categoryList;
      try {
        listData = json['category'] as List<dynamic>;
        categoryList = listData.map((element) => element.toString()).toList();
      } catch (e) {
        categoryList = [];
      }
      List<ServiceLocation> locationList;
      try {
        listData = json['service_locations'] as List<dynamic>;
        locationList = listData
            .map((element) => ServiceLocation.fromJson(element))
            .toList();
      } catch (e) {
        locationList = [];
      }
      return UserData(
        location_lat: double.parse(json['location_lat']),
        location_lon: double.parse(json['location_lon']),
        picture: json['picture'],
        id: idData,
        email: json['email'],
        username: json['username'],
        location: json['location'],
        phone: json['phone'],
        roles: rolesList,
        abn: json['abn'] as String,
        business_type: json['business_type'] as String,
        business_trading_name: json['business_trading_name'] as String,
        service_type: json['service_type'] as String,
        category: categoryList,
        service_locations: locationList,
      );
    }
  }
}

//---------------------------------------------------------
//---------------------------------------------------------
// TaskData
//---------------------------------------------------------
class TaskData {
  int id;
  String title;
  String budget_price;
  String date_start;
  String address;
  double longitude;
  double latitude;
  String user_description;
  String service_type;
  List<String> category;
  int user_id;
  int quotation_id;
  int task_finished;
  String hire_type;
  dynamic picture;

  TaskData({
    this.id,
    this.title,
    this.budget_price,
    this.date_start,
    this.address,
    this.longitude,
    this.latitude,
    this.user_description,
    this.service_type,
    this.category,
    this.user_id,
    this.quotation_id,
    this.task_finished,
    this.hire_type,
    this.picture,
  });

  void printContent({String indent = ''}) {
    print('${indent}id: $id');
    print('${indent}title: $title');
    print('${indent}budget_price: $budget_price');
    print('${indent}address: $address');
    print('${indent}date_start: $date_start');
    print('${indent}longitude: $longitude');
    print('${indent}latitude: $latitude');
    print('${indent}user_description: $user_description');
    print('${indent}service_type: $service_type');
    print('${indent}category:\n');
    category.forEach((element) {
      print('$indent\t\t$element\n');
    });
    print('${indent}user_id: $user_id');
    print('${indent}quotation_id: $quotation_id');
    print('${indent}task_finished: $task_finished');
    print('${indent}hire_type: $hire_type');
    print('${indent}picture: $picture');
  }

  factory TaskData.fromJson(Map<String, dynamic> json) {
    var listData = json['category'] as List<dynamic>;
    List<String> categoryList =
        listData.map((element) => element.toString()).toList();

    return TaskData(
      id: int.parse(json['id']),
      title: json['title'],
      budget_price: json['budget_price'],
      address: json['address'],
      date_start: json['date_start'],
      longitude: double.parse(json['longitude']),
      latitude: double.parse(json['latitude']),
      user_description: json['user_description'],
      service_type: json['service_type'],
      category: categoryList,
      user_id: int.parse(json['user_id']),
      quotation_id: int.parse(json['quotation_id']),
      task_finished: int.parse(json['task_finished']),
      hire_type: json['hire_type'],
      picture: json['picture'],
    );
  }
}

//---------------------------------------------------------
//---------------------------------------------------------
// Quote Data (for quoted/accepted list of customer)
//---------------------------------------------------------
class QuoteData {
  int id;
  int seller_id;
  int task_id;
  String seller_description;
  String quote_price;
  int isAccepted;
  int isDeclined;
  int isNewQuoted;
  int isNewAccepted;
  dynamic picture;
  TaskData task;
  UserData seller;

  QuoteData({
    this.id,
    this.seller_id,
    this.task_id,
    this.seller_description,
    this.quote_price,
    this.isAccepted,
    this.isDeclined,
    this.isNewQuoted,
    this.isNewAccepted,
    this.picture,
    this.task,
    this.seller,
  });

  void printContent({String indent = ''}) {
    print('${indent}id: $id');
    print('${indent}seller_id: $seller_id');
    print('${indent}task_id: $task_id');
    print('${indent}seller_description: $seller_description');
    print('${indent}quote_price: $quote_price');
    print('${indent}isAccepted: $isAccepted');
    print('${indent}isDeclined: $isDeclined');
    print('${indent}isNewQuoted: $isNewQuoted');
    print('${indent}isNewAccepted: $isNewAccepted');
    print('${indent}picture: $picture');
    print('${indent}task:\n');
    task.printContent(indent: indent + '\t\t');
    print('${indent}seller:\n');
    seller.printContent(indent: indent + '\t\t');
  }

  factory QuoteData.fromJson(Map<String, dynamic> json) {
    return QuoteData(
      id: int.parse(json['id']),
      seller_id: int.parse(json['seller_id']),
      task_id: int.parse(json['task_id']),
      seller_description: json['seller_description'],
      quote_price: json['quote_price'],
      isAccepted: int.parse(json['isAccepted']),
      isDeclined: int.parse(json['isDeclined']),
      isNewQuoted: int.parse(json['isNewQuoted']),
      isNewAccepted: int.parse(json['isNewAccepted']),
      picture: json['picture'],
      task: TaskData.fromJson(json['task']),
      seller: UserData.fromJson(json['seller']),
    );
  }
}

//---------------------------------------------------------
//---------------------------------------------------------
// Quote Data (for enquiry list of supplier)
//---------------------------------------------------------
class OpportunityData {
  int id;
  String title;
  String address;
  String date_start;
  String service_type;
  String budget_price;
  String user_description;
  double latitude;
  double longitude;
  int task_finished;
  int quotation_id;
  String hire_type;
  dynamic picture;
  UserData user;
  List<String> category;

  OpportunityData({
    this.id,
    this.title,
    this.address,
    this.date_start,
    this.service_type,
    this.budget_price,
    this.user_description,
    this.latitude,
    this.longitude,
    this.task_finished,
    this.quotation_id,
    this.hire_type,
    this.picture,
    this.user,
    this.category,
  });

  void printContent({String indent = ''}) {
    print('${indent}id: $id');
    print('${indent}title: $title');
    print('${indent}address: $address');
    print('${indent}date_start: $date_start');
    print('${indent}service_type: $service_type');
    print('${indent}budget_price: $budget_price');
    print('${indent}user_description: $user_description');
    print('${indent}latitude: $latitude');
    print('${indent}longitude: $longitude');
    print('${indent}task_finished: $task_finished');
    print('${indent}quotation_id: $quotation_id');
    print('${indent}hire_type: $hire_type');
    print('${indent}picture: $picture');
    print('${indent}user:\n');
    user.printContent(indent: indent + '\t\t');
    print('${indent}category:\n');
    category.forEach((element) {
      print('$indent\t\t$element\n');
    });
  }

  factory OpportunityData.fromJson(Map<String, dynamic> json) {
    var listData = json['category'] as List<dynamic>;
    List<String> categoryList =
        listData.map((element) => element.toString()).toList();

    return OpportunityData(
      id: int.parse(json['id']),
      title: json['title'],
      address: json['address'],
      date_start: json['date_start'],
      service_type: json['service_type'],
      budget_price: json['budget_price'],
      user_description: json['user_description'],
      latitude: double.parse(json['latitude']),
      longitude: double.parse(json['longitude']),
      task_finished: int.parse(json['task_finished']),
      quotation_id: int.parse(json['quotation_id']),
      hire_type: json['hire_type'],
      picture: json['picture'],
      user: UserData.fromJson(json['user']),
      category: categoryList,
    );
  }
}

//---------------------------------------------------------
//---------------------------------------------------------
// Quote Data (for quoted/accepted list of supplier)
//---------------------------------------------------------
class QuotedOpportunityData {
  int id;
  String seller_description;
  String quote_price;
  int isAccepted;
  int isDeclined;
  int isNewQuoted;
  int isNewAccepted;
  dynamic picture;
  UserData user;
  TaskData task;

  QuotedOpportunityData({
    this.id,
    this.seller_description,
    this.quote_price,
    this.isAccepted,
    this.isDeclined,
    this.isNewQuoted,
    this.isNewAccepted,
    this.picture,
    this.user,
    this.task,
  });

  void printContent({String indent = ''}) {
    print('${indent}id: $id');
    print('${indent}seller_description: $seller_description');
    print('${indent}quote_price: $quote_price');
    print('${indent}isAccepted: $isAccepted');
    print('${indent}isDeclined: $isDeclined');
    print('${indent}isNewQuoted: $isNewQuoted');
    print('${indent}isNewAccepted: $isNewAccepted');
    print('${indent}picture: $picture');
    print('${indent}user:\n');
    user.printContent(indent: indent + '\t\t');
    print('${indent}task:\n');
    task.printContent(indent: indent + '\t\t');
  }

  factory QuotedOpportunityData.fromJson(Map<String, dynamic> json) {
    return QuotedOpportunityData(
      id: int.parse(json['id']),
      seller_description: json['seller_description'],
      quote_price: json['quote_price'],
      isAccepted: int.parse(json['isAccepted']),
      isDeclined: int.parse(json['isDeclined']),
      isNewQuoted: int.parse(json['isNewQuoted']),
      isNewAccepted: int.parse(json['isNewAccepted']),
      picture: json['picture'],
      user: UserData.fromJson(json['user']),
      task: TaskData.fromJson(json['task']),
    );
  }
}

//---------------------------------------------------------
//---------------------------------------------------------
// Notification data
//---------------------------------------------------------
class NotificationData {
  int id;
  String data_type; // 'quoted', 'enquiry', 'declined', 'accepted', 'finished'
  int task_id;
  int quote_id;
  String user_name;
  String seller_name;
  String task_title;
  dynamic task_picture;
  dynamic seller_picture;
  String task_location;
  int time_stamp;

  NotificationData({
    this.id,
    this.data_type,
    this.task_id,
    this.quote_id,
    this.user_name,
    this.seller_name,
    this.task_title,
    this.task_picture,
    this.seller_picture,
    this.task_location,
    this.time_stamp,
  });

  void printContent({String indent = ''}) {
    print('${indent}data_type: $data_type');
    print('${indent}task_id: $task_id');
    print('${indent}quote_id: $quote_id');
    print('${indent}user_name: $user_name');
    print('${indent}seller_name: $seller_name');
    print('${indent}task_title: $task_title');
    print('${indent}task_picture: $task_picture');
    print('${indent}seller_picture: $seller_picture');
    print('${indent}task_location: $task_location');
    print('${indent}time_stamp: $time_stamp');
  }

  factory NotificationData.fromMessage(Map<String, dynamic> message) {
    if (Platform.isIOS) {
      return NotificationData(
        id: 0,
        data_type: message['data_type'],
        task_id: int.parse(message['task_id']),
        quote_id: int.parse(message['quote_id']) ?? 0,
        user_name: message['user_name'],
        seller_name: message['seller_name'],
        task_title: message['task_title'],
        task_picture: message['task_picture'],
        seller_picture: message['seller_picture'],
        task_location: message['task_location'],
        time_stamp: int.parse(message['time_stamp']),
      );
    } else {
      return NotificationData(
          id: 0,
          data_type: message['data']['data_type'],
          task_id: int.parse(message['data']['task_id']),
          quote_id: int.parse(message['data']['quote_id']) ?? 0,
          user_name: message['data']['user_name'],
          seller_name: message['data']['seller_name'],
          task_title: message['data']['task_title'],
          task_picture: message['data']['task_picture'],
          seller_picture: message['data']['seller_picture'],
          task_location: message['data']['task_location'],
          time_stamp: int.parse(message['data']['time_stamp']));
    }
  }

  factory NotificationData.fromJson(Map<String, dynamic> json) {
    return NotificationData(
      id: int.parse(json['id']),
      data_type: json['data_type'],
      task_id: int.parse(json['task_id']),
      quote_id: int.parse(json['quote_id']) ?? 0,
      user_name: json['user_name'],
      seller_name: json['seller_name'],
      task_title: json['task_title'],
      task_picture: json['task_picture']=='0' ? false : json['task_picture'],
      seller_picture: json['seller_picture']=='0' ? false : json['seller_picture'],
      task_location: json['task_location'],
      time_stamp: int.parse(json['time_stamp']),
    );
  }
}
