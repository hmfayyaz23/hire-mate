import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'global.dart';

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Check valid email address
//-----------------------------------------------
class ProgressHUD extends StatelessWidget {
  final Widget child;
  final bool inAsyncCall;

  ProgressHUD({
    this.child,
    this.inAsyncCall,
  });

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: inAsyncCall,
      color: Colors.black,
      opacity: 0.35,
      progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(GLOBAL_THEME_COLOR)
      ),
      child: child,
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Show error alert message
//-----------------------------------------------
void nullFunc() {}
Future<void> libShowErrorAlert(BuildContext context, String message,
    {Function funcCallBack = nullFunc}) async {
  await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Hire Mate',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: GLOBAL_THEME_COLOR,
              ),
            ),
          ),
          Divider(color: GLOBAL_THEME_COLOR.withOpacity(0.5)),
        ],
      ),
      content: Text(message),
      actions: <Widget>[
        FlatButton(
            onPressed: () {
              Navigator.pop(context);
              funcCallBack();
            },
            child: Text(
              'Close',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            )),
      ],
    ),
  );
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Show error alert message
//-----------------------------------------------
Future<void> libShowSelectionAlert(
  BuildContext context,
  String message, {
  String textYes = 'OK',
  String textNo = 'Cancel',
  Function funcYes = nullFunc,
}) async {
  await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Hire Mate',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: GLOBAL_THEME_COLOR,
              ),
            ),
          ),
          Divider(color: GLOBAL_THEME_COLOR.withOpacity(0.5)),
        ],
      ),
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(
            textNo,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ),
        FlatButton(
          onPressed: () {
            Navigator.pop(context);
            funcYes();
          },
          child: Text(
            textYes,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ),
      ],
    ),
  );
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Delay second function
//-----------------------------------------------
Future<void> delaySecond(int sec) async {
  await Future.delayed(Duration(seconds: sec));
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Check valid email address
//-----------------------------------------------
void printLog(dynamic value) {
  if (isPrintLog) print(value);
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Print pretty Json string
//-----------------------------------------------
void prettyPrintJson(String input) {
  const JsonDecoder decoder = JsonDecoder();
  const JsonEncoder encoder = JsonEncoder.withIndent('  ');
  final dynamic object = decoder.convert(input);
  final dynamic prettyString = encoder.convert(object);
  prettyString.split('\n').forEach((dynamic element) => printLog(element));
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Check valid email address
//-----------------------------------------------
//bool libCheckValidEmail(String inputString) {
//  return RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
//      .hasMatch(inputString);
//}
bool libCheckValidEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  return (!regex.hasMatch(value)) ? false : true;
}