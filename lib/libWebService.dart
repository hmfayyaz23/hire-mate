import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import 'global.dart';
import 'libMyLib.dart';
import 'libWebData.dart';

const String BASE_DOMAIN = 'https://www.hire-mate.com.au/';
const String BASE_URL = BASE_DOMAIN + 'wp-json/api/v1/';

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Network helper
//-----------------------------------------------
class WebService {
  int userID = 0;
  String token = '';
  String userPassword = '';
  String notificationID = '';

  //---------------------------------------------
  //---- Login Method ----
  //---------------------------------------------
  Future<void> login({
    @required String username,
    @required String password,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> start login...');
      await http
          .post(
        BASE_DOMAIN + 'wp-json/jwt-auth/v1/token',
        headers: {
          "Content-Type": "application/json",
        },
        body: json.encode({
          'username': username,
          'password': password,
        }),
      )
          .then((response) async {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);
        if (statusCode < 200 || statusCode > 400 || json == null) {
          /*
          {
            "code": "[jwt_auth] incorrect_password",
            "message": "ERROR: The username or password you entered is incorrect. ?",
            "data": {
              "status": 403
            }
          }
          */
          throw Exception(json['message'] ?? 'Data Error!');
        } else {
          token = json['token'];
          userID = json['id'];
          userPassword = password;

          List<dynamic> roleList = json['roles'];
          if (roleList.contains('contractor')) {
            await getSeller();
            await getUserTask();
            await getSellerTask();
          } else {
            await getUser();
            await getUserTask();
          }
          await getNotification();
          funcSuccess();
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Get user data
  //---------------------------------------------
  Future<void> getUser({
    int id,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> get user data...');
      await http
          .post(
        BASE_URL + 'user/get',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': id ?? userID,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else
          globalUserData = UserData.fromJson(json);
        funcSuccess();
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Get seller data
  //---------------------------------------------
  Future<void> getSeller({
    int id,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> get seller data...');
      await http
          .post(
        BASE_URL + 'seller/get',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': id ?? userID,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else
          globalUserData = UserData.fromJson(json);
        funcSuccess();
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Create user task
  //---------------------------------------------
  Future<void> createUserTask({
    int id,
    String imageBase64 = '',
    String hire_type = 'Wet Hire',
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> create user task...');
      await http
          .post(
        BASE_URL + 'user/post_new_task',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': id ?? userID,
          'title': globalNewJobData.title,
          'date_start': globalNewJobData.date_start,
          'address': globalNewJobData.address,
          'latitude': globalNewJobData.latitude.toString(),
          'longitude': globalNewJobData.longitude.toString(),
          'budget_price': globalNewJobData.budget_price,
          'service_type': globalNewJobData.service_type,
          'category': globalNewJobData.category,
          'user_description': globalNewJobData.user_description,
          'hire_type': hire_type,
          'picture': imageBase64,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['status'] == 'success')
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Get user task
  //---------------------------------------------
  Future<void> getUserTask({
    int id,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> get user task...');
      await http.post(
        BASE_URL + 'user/get_tasks',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
      ).then((response) {
        //prettyPrintJson(response.body.toString());
        //log(response.body);
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          //-- enquires list --
//          List list = json['enquires'];
//          List<TaskData> listEnquires = [];
//          var i = 0;
//          list.forEach((element) {
//            print('$i---------------------');
//            print(element);
//            TaskData dataEnquires = TaskData.fromJson(element);
//            print('$i=====================');
//            dataEnquires.printContent();
//            listEnquires.add(dataEnquires);
//            i++;
//          });
          List list_1 = json['enquires'];
          List<TaskData> listEnquires =
              list_1.map((e) => TaskData.fromJson(e)).toList();

          //-- quoted list --
//          List list = json['quoted'];
//          List<QuoteData> listQuoted = [];
//          var i = 0;
//          list.forEach((element) {
//            print('$i ------------------------------');
//            print(element['id']);
//            //print(element);
//            QuoteData quoteData = QuoteData.fromJson(element);
//            //quoteData.printContent();
//            listQuoted.add(quoteData);
//            i++;
//          });
          List list_2 = json['quoted'];
          List<QuoteData> listQuoted = list_2.map((e) => QuoteData.fromJson(e)).toList();

          //-- accept list
          List list_3 = json['accepted'];
          List<QuoteData> listAccepted =
              list_3.map((e) => QuoteData.fromJson(e)).toList();

          globalUserEnquiresList = listEnquires;
          globalUserQuotedList = listQuoted;
          globalUserAcceptedList = listAccepted;

          funcSuccess();
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Get seller task
  //---------------------------------------------
  Future<void> getSellerTask({
    int id,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> get seller task...');
      await http.post(
        BASE_URL + 'seller/get_tasks',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
      ).then((response) {
        //prettyPrintJson(response.body.toString());
        //log(response.body);
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          List list_1 = json['enquires'];
          List<OpportunityData> listEnquires =
              list_1.map((e) => OpportunityData.fromJson(e)).toList();

          //-- quoted list --
          List list_2 = json['quoted'];
          List<QuotedOpportunityData> listQuoted =
              list_2.map((e) => QuotedOpportunityData.fromJson(e)).toList();

          //-- accept list
          List list_3 = json['accepted'];
          List<QuotedOpportunityData> listAccepted =
              list_3.map((e) => QuotedOpportunityData.fromJson(e)).toList();

          globalSupplierEnquiresList = listEnquires;
          globalSupplierQuotedList = listQuoted;
          globalSupplierAcceptedList = listAccepted;

          funcSuccess();
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Get notification
  //---------------------------------------------
  Future<void> getNotification({
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> get notification...');
      await http.post(
        BASE_URL + 'user/get_notification',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
      ).then((response) {
        //prettyPrintJson(response.body.toString());
        //log(response.body);
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          List list_1 = json['result'];
          List<NotificationData> listResult =
          list_1.map((e) => NotificationData.fromJson(e)).toList();

          globalNotificationList = listResult;

          funcSuccess();
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Delete user task
  //---------------------------------------------
  Future<void> deleteUserTask({
    int taskID,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> delete user task...');
      await http
          .post(
        BASE_URL + 'user/delete_task',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': taskID,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1)
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Delete notification
  //---------------------------------------------
  Future<void> deleteNotification({
    int taskID,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> delete notification...');
      await http
          .post(
        BASE_URL + 'user/delete_notification',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': taskID,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1)
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- create user task
  //---------------------------------------------
  Future<void> createUser({
    bool isSeller = false,
    Function funcSuccess = nullFunc,
  }) async {
    Map<String, dynamic> body;
    body = {
      'username': globalProfileData.username,
      'password': globalProfilePassword,
      'password_again': globalProfilePassword,
      'email': globalProfileData.email,
      'phone': globalProfileData.phone,
      'location': globalProfileData.location,
      'location_lat': globalProfileData.location_lat,
      'location_lon': globalProfileData.location_lon,
    };
    if (globalProfilePictureBase64.isNotEmpty)
      body['picture'] = globalProfilePictureBase64;
    // Seller only data
    if (isSeller) {
      body['abn'] = globalProfileData.abn;
      body['business_type'] = globalProfileData.business_type;
      body['business_trading_name'] = globalProfileData.business_trading_name;
      body['service_type'] = globalProfileData.service_type;
      //--
      List<String> listCategory = [];
      globalProfileData.category.forEach((element) {
        listCategory.add(element);
      });
      body['category'] = listCategory;
      //--
      List<Map<String, dynamic>> listServiceLocations = [];
      globalProfileData.service_locations.forEach((element) {
        listServiceLocations.add(element.getMap());
      });
      body['service_locations'] = listServiceLocations;
    }

    try {
      print('http ==> register ${isSeller ? 'seller' : 'user'}...');
      await http
          .post(
        BASE_URL + (isSeller ? 'seller' : 'user') + '/create',
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode(body),
      )
          .then((response) async {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception(json['message'] ?? 'Data Error!');
        else {
          if (json['id'] != null) {
            // update user password
            funcSuccess();
          } else
            throw Exception(json['message'] ?? 'Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- update user task
  //---------------------------------------------
  Future<void> updateUser({
    Function funcSuccess = nullFunc,
    String password = '',
  }) async {
    Map<String, dynamic> body;
    if (password.isEmpty) {
      body = {
        'username': globalProfileData.username,
        'phone': globalProfileData.phone,
        'location': globalProfileData.location,
        'location_lat': globalProfileData.location_lat,
        'location_lon': globalProfileData.location_lon,
      };
      if (globalProfilePictureBase64.isNotEmpty)
        body['picture'] = globalProfilePictureBase64;
      // Seller only data
      if (globalProfileData.isSeller()) {
        body['abn'] = globalProfileData.abn;
        body['business_type'] = globalProfileData.business_type;
        body['business_trading_name'] = globalProfileData.business_trading_name;
        body['service_type'] = globalProfileData.service_type;
        //--
        List<String> listCategory = [];
        globalProfileData.category.forEach((element) {
          listCategory.add(element);
        });
        body['category'] = listCategory;
        //--
        List<Map<String, dynamic>> listServiceLocations = [];
        globalProfileData.service_locations.forEach((element) {
          listServiceLocations.add(element.getMap());
        });
        body['service_locations'] = listServiceLocations;
      }
    } else {
      // only password update...
      body = {
        'password': password,
        'password_again': password,
      };
    }

    try {
      print('http ==> update user profile...');
      await http
          .post(
        BASE_URL + (globalUserData.isSeller() ? 'seller' : 'user') + '/update',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode(body),
      )
          .then((response) async {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          // update user password data is password is updated
          if (password.isNotEmpty) userPassword = password;

          if (globalUserData.isSeller())
            await getSeller(funcSuccess: funcSuccess);
          else
            await getUser(funcSuccess: funcSuccess);
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Submit Quote
  //---------------------------------------------
  Future<void> submitQuote({
    @required int task_id,
    String seller_description = '',
    String quote_price = '',
    String imageBase64 = '',
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> submit quotation...');
      await http
          .post(
        BASE_URL + 'seller/submit_quotation',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'task_id': task_id,
          'seller_description': seller_description,
          'quote_price': quote_price,
          'picture': imageBase64,
        }),
      )
          .then((response) {
        prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['status'] == 'success')
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Delete Quote
  //---------------------------------------------
  Future<void> deleteQuote({
    @required int id,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> delete quotation...');
      await http
          .post(
        BASE_URL + 'seller/delete_quotation',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': id,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1)
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------`````````````````````
  //---- Decline Quote
  //---------------------------------------------
  Future<void> declineQuote({
    @required int id,
    Function funcSuccess = nullFunc,
  }) async {
    print('wilson: $id');
    try {
      print('http ==> decline quotation...');
      await http
          .post(
        BASE_URL + 'user/decline_quotation',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': id,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1)
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Accept Quote
  //---------------------------------------------
  Future<void> acceptQuote({
    @required int id,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> accept quotation...');
      await http
          .post(
        BASE_URL + 'user/accept_quotation',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': id,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1)
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Update Quote
  //---------------------------------------------
  Future<void> updateQuote({
    @required int id,
    int isNewAccepted = 9999,
    int isNewQuoted = 9999,
    Function funcSuccess = nullFunc,
  }) async {
    Map<String, dynamic> body = {
      'id': id,
    };
    if (isNewAccepted != 9999) body['isNewAccepted'] = isNewAccepted;
    if (isNewQuoted != 9999) body['isNewQuoted'] = isNewQuoted;

    try {
      print('http ==> update quotation...');
      await http
          .post(
        BASE_URL + 'seller/update_quotation',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode(body),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1)
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Finish task
  //---------------------------------------------
  Future<void> finishTask({
    @required int id,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> finish task...');
      await http
          .post(
        BASE_URL + 'user/finish_task',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'id': id,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1)
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- reset password
  //---------------------------------------------
  Future<void> resetPassword({
    @required String email,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> reset password...');
      await http
          .post(
        BASE_URL + 'reset-password',
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          'email': email,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['status'] == 'success')
            funcSuccess();
          else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Set Android id
  //---------------------------------------------
  Future<void> setAndroidID({
    @required String regID,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> set Android registration id...');
      await http
          .post(
        BASE_URL + 'android/add_registration_id',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'registration_id': regID,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1) {
            notificationID = regID;
            funcSuccess();
          } else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  //---- Clear Android id
  //---------------------------------------------
  Future<void> clrAndroidID({
    @required String regID,
    Function funcSuccess = nullFunc,
  }) async {
    try {
      print('http ==> clear Android registration id...');
      await http
          .post(
        BASE_URL + 'android/delete_registration_id',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer' + token,
        },
        body: json.encode({
          'registration_id': regID ?? notificationID,
        }),
      )
          .then((response) {
        //prettyPrintJson(response.body.toString());
        final int statusCode = response.statusCode;
        final json = jsonDecode(response.body);

        if (statusCode < 200 || statusCode > 400 || json == null)
          throw Exception('Data Error!');
        else {
          if (json['result'] == 1) {
            notificationID = '';
            funcSuccess();
          } else
            throw Exception('Data Error!');
        }
      });
    } catch (e) {
      print('http error ==> $e');
      rethrow;
    }
  }
} // class WebService
