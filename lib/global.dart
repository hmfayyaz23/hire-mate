import 'package:flutter/material.dart';

import 'libWebData.dart';
import 'libWebService.dart';

//---------------------------------------------------------
//---------------------------------------------------------
//---------------------------------------------------------
//-- Global Constants --
//---------------------------------------------------------
const bool isPrintLog = true;

const String GLOBAL_MAP_KEY = 'AIzaSyCrkjlpYYhjaMFq4i6CQrznyw7x8zmPFH0';

const GLOBAL_THEME_COLOR = Color(0xffd3192e);

const Map<String, List<String>> GLOBAL_SERVICES = {
  'Hire/Rental': [
    "cat-----Plant & Equip",
    "Excavator",
    "Compaction Roller",
    "Wheel Loader",
    "Skid Steer (Mini Loader)",
    "Dozers",
    "Motor Grader",
    "Backhoe Dump Truck",
    "Water Truck",
    "Firefighting water trailers",
    "Fuel Trailer",
    "Mobile Crusher",
    "Mobile Screen",
    "Telehandler",
    "VMS board",
    "Tractor",
    "cat-----Access gear",
    "Scissor Lift",
    "Ladder",
    "Boom Lift",
    "Knuckle Boom",
    "cat-----Trucks and Cars",
    "Prime Movers",
    "Trailers - Trucks",
    "Trailers - Cars",
    "Trucks",
    "Vans",
    "Utes",
    "Cars",
    "cat-----Tools & Small Plant",
    "Air Compressor",
    "Generator",
    "Trolley",
    "Lawn mover",
    "Chain saw",
    "Nail gun",
    "Welder",
    "Trolley",
    "Lighting",
    "Jack hammer",
    "Cleaning and scrubbing",
    "Concrete Mixer",
    "Wood Working Gear",
  ],

  'Transport' : [
    "Plant/Machinery",
    "Pallet ",
    "Shipping containers",
    "Motorbikes",
    "General Freight",
    "Livestock",
    "Bulk Haulage",
    "Hay",
    "Boats",
    "Caravans",
    "Portable Building",
    "Refrigerated Trucks",
    "Pilot",
  ],

  'Tradie' : [
    "Air-conditioning ",
    "Welder",
    "Bricklayer",
    "Builder",
    "Cabinetmaker",
    "Carpenter",
    "Carpet Installer",
    "Concreter",
    "Electrician",
    "Fencer/Fence Erector",
    "Flooring Installer",
    "Glazier",
    "Labourer",
    "Landscaper",
    "Locksmith",
    "Motor Mechanics",
    "Painter",
    "Panel Beater",
    "Plasterer",
    "Plumber (General)",
    "Roof Tiler",
    "Sheet Metal Worker",
    "Signwriter",
  ],
};

// init location is Melbourne
const double GLOBAL_INIT_LAT = -37.840935;
const double GLOBAL_INIT_LNG = 144.946457;

const Color globalBackgroundColorHUD = Colors.transparent;
const Color globalBorderColorHUD = Colors.transparent;
const Color globalBarrierColorHUD = Colors.black38;
const Color globalIndicatorColorHUD = GLOBAL_THEME_COLOR;

//---------------------------------------------------------
//---------------------------------------------------------
//---------------------------------------------------------
//-- Global Variables --
//---------------------------------------------------------
String globalVersionNumber;
String globalBuildNumber;

int globalAcceptedCustomerIndex;
int globalAcceptedSupplierIndex;

double globalOffsetScMeCustomer;
double globalOffsetScMeSupplier;

WebService globalWebService = WebService();

UserData globalUserData;
UserData globalProfileData;
String globalProfilePictureBase64;
String globalProfilePassword;

List<TaskData> globalUserEnquiresList;
List<QuoteData> globalUserQuotedList;
List<QuoteData> globalUserAcceptedList;

List<OpportunityData> globalSupplierEnquiresList;
List<QuotedOpportunityData> globalSupplierQuotedList;
List<QuotedOpportunityData> globalSupplierAcceptedList;

TaskData globalNewJobData;

List<NotificationData> globalNotificationList = [];

//-----------------------------------------------
//-----------------------------------------------
class NotificationModel with ChangeNotifier {
  int countCustomer;
  int countSupplier;
  int countTotal;

  NotificationModel({
    this.countCustomer = 0,
    this.countSupplier = 0,
    this.countTotal = 0,
  });

  void setNewNotification() {
    print('set model');
    countCustomer = 0;
    countSupplier = 0;
    globalNotificationList.forEach((element) {
      if (element.data_type == 'quoted')
        countCustomer++;
      else
        countSupplier++;
    });
    countTotal = countCustomer + countSupplier;
    notifyListeners();
  }

  void testing() {
    countCustomer--;
    countSupplier--;
    countTotal -= 2;
    notifyListeners();
  }
}
