import 'package:flutter/material.dart';
import 'package:hire_mate_app/scenes/scQuote.dart';

import 'package:provider/provider.dart';

import '../global.dart';
import '../libWebData.dart';
import '../objects/objInfoCard.dart';
import '../objects/objTextDateFormat.dart';

class ScNotificationsCustomer extends StatefulWidget {
  final NotificationModel mo;
  ScNotificationsCustomer({@required this.mo});

  @override
  _ScNotificationsCustomerState createState() =>
      _ScNotificationsCustomerState();
}

class _ScNotificationsCustomerState extends State<ScNotificationsCustomer> {
  // RefreshIndicator requires key to implement state (for RefreshIndicator)
  final GlobalKey<RefreshIndicatorState> _refreshKey =
      GlobalKey<RefreshIndicatorState>();

  List<NotificationData> notificationList;

  bool _isUpdating = false;
  //---------------------------------------------
  //---------------------------------------------
  void updateNotificationList() {
    notificationList = [];
    globalNotificationList.forEach((element) {
      //element.printContent();
      if (element.data_type == 'quoted') notificationList.add(element);
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<Null> _refresh() async {
    _isUpdating = true;

    // waiting to load notification list
    //await new Future.delayed(new Duration(seconds: 2));
    try {
      int count = globalNotificationList.length;
      await globalWebService.getNotification();
      widget.mo.setNewNotification();

      if (globalNotificationList.length != count) {
        await globalWebService.getUserTask();
        await globalWebService.getSellerTask();
      }
    } catch (e) {
      print(e);
    }
    _isUpdating = false;
    updateNotificationList();

    setState(() {});

    return null;
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScNotificationsCustomer]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScNotificationsCustomer]didChangeDependencies...');

    updateNotificationList();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScNotificationsCustomer]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        key: _refreshKey,
        onRefresh: _refresh,
        child: ListView.builder(
          itemCount: notificationList.length,
          itemBuilder: (context, index) {
            return InfoCardNotifications(
              imgLink: notificationList[index].seller_picture,
              name: notificationList[index].seller_name,
              timeLog: getTimeStampString(notificationList[index].time_stamp),
              status:
                  'Quotation Received - ${notificationList[index].task_title}',
              onTap: () async {
                //-- delete and update notification --
                try {
                  globalWebService.deleteNotification(
                      taskID: notificationList[index].id,
                      funcSuccess: () {
                        globalWebService.getNotification(funcSuccess: () {
                          widget.mo.setNewNotification();
                        });
                      });
                } catch (e) {
                  print(e);
                }

                //-- go to the quote detail --
                QuoteData quoteData;
                globalUserQuotedList.forEach((element) {
                  if (element.id == notificationList[index].quote_id &&
                      element.isDeclined == 0 &&
                      element.isAccepted == 0) quoteData = element;
                });
                if (quoteData != null) {
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ScQuote(
                              quoteData: quoteData,
                            )),
                  );
                }

                //-- come back and update the list --
                //updateNotificationList();
                notificationList.removeAt(index);
                setState(() {});
              },
            );
          },
        ),
      ),
    );
  }
}
