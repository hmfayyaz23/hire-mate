import 'package:flutter/material.dart';

import '../global.dart';
import '../objects/objAppButton.dart';

const String msgNewTask  = 'Your new job is successfully submitted. Once contractor send you a quotation you will see in notification.';
const String msgNewQuote = 'Your price is successfully submitted. Once customer approve it you will get an notification.';

//---------------------------------------------------------
//---------------------------------------------------------
class ScSuccess extends StatelessWidget {
  final bool isQuotation;
  ScSuccess({
    this.isQuotation = false,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        //-------------------
        //-- Title App Bar --
        //-------------------
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          title: Text(
            isQuotation ? 'Quotation' : 'New job',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          centerTitle: true,
        ),
        body: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //-- Success Icon --
              CircleAvatar(
                radius: 80,
                child: Icon(Icons.done,
                  size: 120,
                  color: Colors.white,
                ),
                backgroundColor: GLOBAL_THEME_COLOR,
              ),
              SizedBox(height: 30),
              //-- congratulation --
              Text(
                'Congratulation!',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              //-- Message --
              Container(
                width: 280,
                child: Text(
                  isQuotation ? msgNewQuote : msgNewTask,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
              SizedBox(height: 30),
              //-------------------------
              //-- Submit Quote button --
              //-------------------------
              AppButton(
                title: isQuotation ? 'My opportunities' : 'My jobs',
                onPressed: () {
                  Navigator.popUntil(context, ModalRoute.withName('Menu'));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
