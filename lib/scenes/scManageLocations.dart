import 'package:flutter/material.dart';

import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../global.dart';
import '../libMyLib.dart';
import '../libWebData.dart';
import '../libMapTools.dart';
import '../objects/objEntryField.dart';

import 'scShowMap.dart';
//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
class ScManageLocations extends StatefulWidget {
  final List<ServiceLocation> listServiceLocation;

  ScManageLocations({
    @required this.listServiceLocation,
  });

  @override
  _ScManageLocationsState createState() => _ScManageLocationsState();
}

class _ScManageLocationsState extends State<ScManageLocations> {
  String _inputAddress = '';
  String _inputRange = '';
  double _inputLat = 0;
  double _inputLon = 0;

  List<ServiceLocation> listCopyServiceLocation = [];
  final FocusNode _focusRange = FocusNode();
  final TextEditingController _addressController = TextEditingController();

  //---------------------------------
  //-- Bottom Sheet Build Function --
  //---------------------------------
  Widget _bottomSheet(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        // to avoid keyboard covered
        padding: EdgeInsets.only(
          top: 15,
          bottom: MediaQuery.of(context).viewInsets.bottom,
          left: 15,
          right: 15,
        ),
        child: Column(
          children: <Widget>[
            //-------------------
            //-- Address Field --
            //-------------------
            EntryField(
              readOnly: true,
              onTap: () {
                double lat = (_inputLat == 0) ? GLOBAL_INIT_LAT : _inputLat;
                double lng = (_inputLon == 0) ? GLOBAL_INIT_LNG : _inputLon;
                bool isToMyLocation = (_inputLat == 0 && _inputLon == 0);
                setState(() async {
                  LocationResult result = await showLocationPicker(
                    context,
                    GLOBAL_MAP_KEY,
                    initialCenter: LatLng(lat, lng),
                    automaticallyAnimateToCurrentLocation: isToMyLocation,
                    myLocationButtonEnabled: false,
                    requiredGPS: false,
                    layersButtonEnabled: false,
                    appBarColor: Colors.black26,
                  );
                  _addressController.text = result.address;
                  _inputAddress = result.address;
                  _inputLat = result.latLng.latitude;
                  _inputLon = result.latLng.longitude;
                });
              },
              label: 'Address',
//              onChanged: (text) {
//                _inputAddress = text;
//              },
//              onSubmitted: (value) {
//                FocusScope.of(context).requestFocus(_focusRange);
//              },
              controller: _addressController,
              textInputAction: TextInputAction.next,
            ),
            //----------------------
            //-- Locate Me Button --
            //----------------------
//            Align(
//              alignment: Alignment.centerRight,
//              child: FlatButton(
//                onPressed: () async {
//                  LocationResult result = await showLocationPicker(
//                    context,
//                    GLOBAL_MAP_KEY,
//                    initialCenter: LatLng(GLOBAL_INIT_LAT, GLOBAL_INIT_LNG),
//                    automaticallyAnimateToCurrentLocation: false,
//                    myLocationButtonEnabled: false,
//                    requiredGPS: false,
//                    layersButtonEnabled: false,
//                    appBarColor: Colors.black26,
//                  );
//
//                  setState(() {
//                    _addressController.text = result.address;
//                    _inputAddress = result.address;
//                    _inputLat = result.latLng.latitude;
//                    _inputLon = result.latLng.longitude;
//                  });
//                },
//                child: Text(
//                  'Locate me',
//                  style: TextStyle(
//                    color: GLOBAL_THEME_COLOR,
//                    fontSize: 16,
//                    decoration: TextDecoration.underline,
//                  ),
//                ),
//              ),
//            ),
            //------------------
            //-- Range Button --
            //------------------
            EntryField(
              label: 'Range (km)',
              onChanged: (text) {
                _inputRange = text;
              },
              onSubmitted: (value) {
                FocusScope.of(context).unfocus();
              },
              focusNode: _focusRange,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: 20),
            //----------------
            //-- Add Button --
            //----------------
            RaisedButton(
              color: GLOBAL_THEME_COLOR,
              onPressed: () async {
                //-- address field
                if (_inputAddress.isEmpty) {
                  await libShowErrorAlert(
                      context, 'Please enter a valid address.');
                  return;
                }
                //-- position data
                if (_inputLat == 0 || _inputLon == 0) {
                  try {
                    LocationData result = await getLocation(_inputAddress);
                    if (!result.isError) {
                      _inputLat = result.lat;
                      _inputLon = result.lng;
                    } else {
                      await libShowErrorAlert(
                          context, 'Please enter a valid location.');
                      return;
                    }
                  } catch (e) {
                    print(e);
                    await libShowErrorAlert(
                        context, 'Please enter a valid location.');
                    return;
                  }
                }
                //-- range data
                try {
                  double distance = double.parse(_inputRange);
                  //print('wilson $_inputAddress');
                  //print('wilson $distance');
                  //print('wilson $_inputLat');
                  //print('wilson $_inputLon');
                  Navigator.of(context)
                      .pop(true); // return true as valid data entry
                } catch (e) {
                  await libShowErrorAlert(
                      context, 'Please enter a valid service range in km.');
                  return;
                }
              },
              child: Container(
                width: 100,
                child: Center(
                  child: Text(
                    'Add',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  //-----------------------------------
  //-----------------------------------
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    widget.listServiceLocation.forEach((element) {
      listCopyServiceLocation.add(ServiceLocation(
        lat: element.lat,
        lon: element.lon,
        addr: element.addr,
        distance: element.distance,
      ));

      element.printContent();
    });
  }

  //-----------------------------------
  //-----------------------------------
  Widget build(BuildContext context) {
    return Scaffold(
      //-------------------
      //-- Title App Bar --
      //-------------------
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        centerTitle: true,
        //-- Back Button --
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        //-- Title --
        title: Text(
          'Manage Locations',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        //-- Save Button --
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: GLOBAL_THEME_COLOR,
              size: 30,
            ),
            onPressed: () {
              Navigator.of(context).pop(listCopyServiceLocation);
            },
          ),
        ],
      ),
      //--------------------------------
      //-- List of Selected Locations --
      //--------------------------------
      body: Column(
        children: <Widget>[
          SizedBox(height: 10),
          Expanded(
            child: ListView.builder(
              itemCount: listCopyServiceLocation.length,
              itemBuilder: (context, index) {
                return Column(
                  children: <Widget>[
                    Slidable(
                      actionPane: SlidableDrawerActionPane(),
                      actionExtentRatio: 0.2,
                      child: ListTile(
                        title: Text(
                          listCopyServiceLocation[index].addr,
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                        subtitle: Text(
                          'Range: ${listCopyServiceLocation[index].distance} km',
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.grey,
                          ),
                        ),
                        //---------------------
                        //-- Data row button --
                        //---------------------
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScShowMap(
                                      distance: listCopyServiceLocation[index]
                                          .distance,
                                      address:
                                          listCopyServiceLocation[index].addr,
                                      location: LatLng(
                                          listCopyServiceLocation[index].lat,
                                          listCopyServiceLocation[index].lon),
                                    )),
                          );
                        },
                      ),
                      secondaryActions: <Widget>[
                        IconSlideAction(
                          caption: 'Delete',
                          color: GLOBAL_THEME_COLOR,
                          icon: Icons.delete,
                          onTap: () async {
                            await libShowSelectionAlert(
                                context, 'Delete selected service location?',
                                textYes: 'Delete',
                                textNo: 'Cancel', funcYes: () {
                              setState(() {
                                listCopyServiceLocation.removeAt(index);
                              });
                            });
                          },
                        ),
                      ],
                    ),
                    Divider(color: GLOBAL_THEME_COLOR),
                  ],
                );
              },
            ),
          ),
        ],
      ),
      //-----------------------------
      //-- Add New Location Button --
      //-----------------------------
      floatingActionButton: SizedBox(
        width: 75,
        height: 75,
        child: FloatingActionButton(
          onPressed: () async {
            // reset input data
            _inputAddress = '';
            _addressController.text = '';
            _inputRange = '';
            _inputLat = 0;
            _inputLon = 0;
            // go to edit at bottom sheet
            var isValidDataReturned = await showModalBottomSheet(
              context: context,
              builder: _bottomSheet,
              isScrollControlled: true,
            );
            // refresh list
            if (isValidDataReturned == true) {
              setState(() {
                listCopyServiceLocation.add(ServiceLocation(
                  addr: _inputAddress,
                  distance: double.parse(_inputRange),
                  lat: _inputLat,
                  lon: _inputLon,
                ));
              });
            }
          },
          child: Icon(Icons.add),
          backgroundColor: GLOBAL_THEME_COLOR,
        ),
      ),
    );
  }

  void dispose() {
    _addressController.dispose();
    super.dispose();
  }
}
