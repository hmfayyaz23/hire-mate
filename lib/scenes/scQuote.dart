import 'package:flutter/material.dart';

import '../global.dart';
import '../libWebData.dart';
import '../libMyLib.dart';
import '../objects/objAppButton.dart';
import '../objects/objCacheImage.dart';
import '../objects/objTextDateFormat.dart';

class ScQuote extends StatefulWidget {
  final QuoteData quoteData;

  ScQuote({
    @required this.quoteData,
  });

  @override
  _ScQuoteState createState() => _ScQuoteState();
}

class _ScQuoteState extends State<ScQuote> {
  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  Future<void> _goDecline() async {
    await libShowSelectionAlert(
        context, 'Reject quotation from ${widget.quoteData.seller.username}?',
        textNo: 'Cancel', textYes: 'Decline', funcYes: () async {
      setState(() {
        _waiting = true;
      });
      try {
        await globalWebService.declineQuote(
            id: widget.quoteData.id,
            funcSuccess: () async {
              // update user task list for the removed task.
              await globalWebService.getUserTask();
              Navigator.of(context).pop();
            });
      } catch (e) {
        setState(() {
          _waiting = false;
        });
        String msg = e.toString();
        int idx = msg.indexOf(':') ?? -2;
        await libShowErrorAlert(context, msg.substring(idx + 2));
      }
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<void> _goAccept() async {
    await libShowSelectionAlert(
        context, 'Accept quotation from ${widget.quoteData.seller.username}?',
        textNo: 'Cancel', textYes: 'Accept', funcYes: () async {
      setState(() {
        _waiting = true;
      });
      try {
        await globalWebService.acceptQuote(
            id: widget.quoteData.id,
            funcSuccess: () async {
              // update user task list for the removed task.
              await globalWebService.getUserTask();
              Navigator.of(context).pop();
            });
      } catch (e) {
        setState(() {
          _waiting = false;
        });
        String msg = e.toString();
        int idx = msg.indexOf(':') ?? -2;
        await libShowErrorAlert(context, msg.substring(idx + 2));
      }
    });
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[scQuote]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[scQuote]didChangeDependencies...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[scQuote]dispose...");
    super.dispose();
  }

  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Container(
          child: Scaffold(
            //-------------------
            //-------------------
            //-- Title App Bar --
            //-------------------
            appBar: AppBar(
              brightness: Brightness.light,
              backgroundColor: Colors.white,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              title: Text(
                'Quote',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              centerTitle: true,
            ),
            body: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  //----------------------------------------
                  //----------------------------------------
                  // -- Middle scrolling view of contents --
                  //----------------------------------------
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          //-----------------------
                          //-- Image --
                          //-----------------------
                          CacheImage(
                            width: MediaQuery.of(context).size.width - 30,
                            height: (MediaQuery.of(context).size.width - 30)*0.75,
                            imageURL: widget.quoteData.picture,
                          ),
                          SizedBox(height: 10),
                          //------------------------------
                          //-- User avatar, name, price --
                          //------------------------------
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              //-- User avatar, name --
                              Row(
                                children: <Widget>[
                                  CacheAvatar(
                                      imageURL:
                                          widget.quoteData.seller.picture),
                                  SizedBox(width: 10),
                                  Text(
                                    widget.quoteData.seller.username,
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                              //-- Quote Price --
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    'Quote price',
                                    style: TextStyle(
                                      fontSize: 14,
                                      //fontWeight: FontWeight.bold,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  SizedBox(height: 2),
                                  Text(
                                    '\$${widget.quoteData.quote_price}',
                                    style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: GLOBAL_THEME_COLOR,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          //SizedBox(height: 10),
                          Divider(color: Colors.grey),
                          //-----------
                          //-- Title --
                          //-----------
                          Text(
                            widget.quoteData.task.title,
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 10),
                          //-----------------------
                          //-- Date and Location --
                          //-----------------------
                          Row(
                            children: <Widget>[
                              //-- Date --
                              TextDateFormat(
                                dateString: widget.quoteData.task.date_start,
                              ),
                              SizedBox(width: 20),
                              //-- Location --
                              Row(
                                children: <Widget>[
                                  Icon(Icons.location_on,
                                      color: GLOBAL_THEME_COLOR, size: 16),
                                  Container(
                                    width: 160,
                                    child: Text(
                                      widget.quoteData.task.address,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: GLOBAL_THEME_COLOR,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(height: 15),
                          //----------------------
                          //-- Task Description --
                          //----------------------
                          Text(
                            'Description:',
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            widget.quoteData.seller_description,
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  //-------------------------------
                  //-------------------------------
                  //-- Accept and Reject buttons --
                  //-------------------------------
                  Column(
                    children: <Widget>[
                      AppButton(
                        title: 'Reject quote',
                        isReverse: true,
                        onPressed: () async {
                          await _goDecline();
                        },
                      ),
                      //SizedBox(height: 10),
                      AppButton(
                        title: 'Accept quote',
                        onPressed: () async {
                          await _goAccept();
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
