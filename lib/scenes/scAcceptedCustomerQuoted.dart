import 'package:flutter/material.dart';

import '../global.dart';
import '../libWebData.dart';
import '../objects/objInfoCard.dart';
import '../objects/objTextDateFormat.dart';

import 'scQuote.dart';

class ScAcceptedCustomerQuoted extends StatefulWidget {
  @override
  _ScAcceptedCustomerQuotedState createState() => _ScAcceptedCustomerQuotedState();
}

class _ScAcceptedCustomerQuotedState extends State<ScAcceptedCustomerQuoted> {
  // RefreshIndicator requires key to implement state (for RefreshIndicator)
  final GlobalKey<RefreshIndicatorState> _refreshKey = GlobalKey<RefreshIndicatorState>();
  // For scrolling control
  ScrollController _scrollController;

  List<QuoteData> _listQuotedData = [];

  bool _isUpdating = false;

  //---------------------------------------------
  //---------------------------------------------
  void _setupQuotedList() {
    _listQuotedData = [];
    // remove any accepted or declined job
    globalUserQuotedList.forEach((element) {
      if (element.isAccepted == 0 && element.isDeclined == 0) _listQuotedData.add(element);
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<Null> _refresh() async {
    _isUpdating = true;
    await globalWebService.getUserTask(funcSuccess: () {
      _setupQuotedList();
      _isUpdating = false;
    });

    setState(() {});

    return null;
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScAcceptedCustomerQuoted]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScAcceptedCustomerQuoted]didChangeDependencies...');

    _setupQuotedList();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScAcceptedCustomerQuoted]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Container(
      child: RefreshIndicator(
        key: _refreshKey,
        onRefresh: _refresh,
        child: ListView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          controller: _scrollController,
          itemCount: _listQuotedData.length,
          itemBuilder: (context, index) {
            return InfoCard(
              imgLink: _listQuotedData[index].picture,
              title: _listQuotedData[index].task.title,
              price: '\$${_listQuotedData[index].quote_price}',
              location: _listQuotedData[index].task.address,
              startDate: getFormattedString(_listQuotedData[index].task.date_start),
              subtitle: _listQuotedData[index].seller_description,
              onTap: () async {
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ScQuote(
                        quoteData: _listQuotedData[index],
                      )),
                );
                setState(() {
                  _setupQuotedList();
                }); // quote list may be modified (declined/accepted)
              },
            );
          },
        ),
      ),
    );
  }
}
