import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';

import 'package:hire_mate_app/libMyLib.dart';

import '../global.dart';
import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';
import '../libGetPicture.dart';

import 'scSuccess.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScNewQuote extends StatefulWidget {
  final int taskID;

  ScNewQuote({
    @required this.taskID,
  });
  @override
  _ScNewQuoteState createState() => _ScNewQuoteState();
}

class _ScNewQuoteState extends State<ScNewQuote> {
  String _inputPrice = '';
  String _inputDescription = '';
  String _imageBase64 = '';

  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  String _selectImageSource = '';
  File _image;

  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  Future<void> _goSubmit() async {
    if (_inputPrice.isEmpty) {
      await libShowErrorAlert(context, 'Please enter propose price.');
      return;
    }
    if (_inputDescription.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a description for your quotation.');
      return;
    }

    setState(() {
      _waiting = true;
    });
    try {
      await globalWebService.submitQuote(
          task_id: widget.taskID,
          quote_price: _inputPrice,
          seller_description: _inputDescription,
          imageBase64: _imageBase64,
          funcSuccess: () async {
            // update seller list for the newly quoted task.
            await globalWebService.getSellerTask();

            setState(() {
              _waiting = false;
            });

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ScSuccess(
                  isQuotation: true,
                ),
              ),
            );
          });
    } catch (e) {
      setState(() {
        _waiting = false;
      });
      String msg = e.toString();
      int idx = msg.indexOf(':') ?? -2;
      await libShowErrorAlert(context, msg.substring(idx + 2));
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget _bottomSheet(context) {
    return Container(
      child: Wrap(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.camera_alt),
              title: Text('Take a photo'),
              onTap: () {
                _selectImageSource = 'camera';
                Navigator.of(context).pop();
              }),
          ListTile(
            leading: Icon(Icons.image),
            title: Text('Image from gallery'),
            onTap: () {
              _selectImageSource = 'gallery';
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  //---------------------------------------------
  //---------------------------------------------
  // Get Image function
  //---------------------------------------------
  Future getImage() async {
    _selectImageSource = '';
    await showModalBottomSheet(context: context, builder: _bottomSheet);
    if (_selectImageSource.isEmpty) return;

    File savedImageFile = await getPicture(selectImageSource: _selectImageSource);

    List<int> imageBytes = savedImageFile.readAsBytesSync();
    _imageBase64 = base64Encode(imageBytes);

    print('Image Size: ${_imageBase64.length}');

    setState(() {
      _image = savedImageFile ?? null;
    });
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Scaffold(
          //-------------------
          //-- Title App Bar --
          //-------------------
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              'Quotation',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
          ),
          //--------------------------
          //-- body of entry fields --
          //--------------------------
          body: Container(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          //-------------------
                          //-- Propose Price --
                          //-------------------
                          EntryField(
                            label: 'Propose Price',
                            prefix: '\$',
                            onChanged: (text) {
                              _inputPrice = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            controller: _priceController,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                          ),
                          SizedBox(height: 30),
                          //---------------------
                          //-- Picture content --
                          //---------------------
                          InkWell(
                            onTap: () => getImage(),
                            child: SizedBox(
                              width: 240,
                              height: 180,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    child: _image == null
                                        ? Container(
                                            width: 240,
                                            height: 180,
                                            color: Color(0x1fff0000),
                                            child: Center(
                                              child: Text(
                                                'Upload photo',
                                                style: TextStyle(
                                                  color: GLOBAL_THEME_COLOR,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                          )
                                        : Container(
                                            width: 240,
                                            height: 180,
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                fit: BoxFit.fill,
                                                image: FileImage(_image),
                                              ),
                                            ),
                                          ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Icon(
                                      Icons.add_photo_alternate,
                                      color: GLOBAL_THEME_COLOR,
                                      size: 50,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          //-----------------------
                          //-- Description field --
                          //-----------------------
                          EntryField(
                            label: 'Description',
                            onChanged: (text) {
                              _inputDescription = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            controller: _descriptionController,
                            keyboardType: TextInputType.multiline,
                            textInputAction: TextInputAction.newline,
                            maxLines: 4,
                          ),
                        ],
                      ),
                    ),
                  ),
                  //------------------
                  //-- Submit button --
                  //------------------
                  AppButton(
                    title: 'Submit',
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      await _goSubmit();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void dispose() {
    _priceController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }
}
