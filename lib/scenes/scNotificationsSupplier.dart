import 'package:flutter/material.dart';
import 'package:hire_mate_app/scenes/scAcceptedSupplierJob.dart';
import 'package:hire_mate_app/scenes/scOpportunity.dart';
import 'package:hire_mate_app/scenes/scQuote.dart';
import 'package:provider/provider.dart';

import '../global.dart';
import '../libWebData.dart';
import '../objects/objInfoCard.dart';
import '../objects/objTextDateFormat.dart';

class ScNotificationsSupplier extends StatefulWidget {
  final NotificationModel mo;
  ScNotificationsSupplier({@required this.mo});

  @override
  _ScNotificationsSupplierState createState() =>
      _ScNotificationsSupplierState();
}

class _ScNotificationsSupplierState extends State<ScNotificationsSupplier> {
  // RefreshIndicator requires key to implement state (for RefreshIndicator)
  final GlobalKey<RefreshIndicatorState> _refreshKey =
      GlobalKey<RefreshIndicatorState>();

  List<NotificationData> listDisplayData;

  bool _isUpdating = false;

  //---------------------------------------------
  //---------------------------------------------
  String getStatus(String dataType) {
    switch (dataType) {
      case 'enquiry':
        return 'New Opportunity';
      case 'declined':
        return 'Your quotation was declined';
      case 'accepted':
        return 'Your quotation was accepted';
      case 'finished':
        return 'Task finished';
    }
    return '';
  }

  //---------------------------------------------
  //---------------------------------------------
  void updateListDisplayData() {
    listDisplayData = [];
    globalNotificationList.forEach((element) {
      if (element.data_type != 'quoted') listDisplayData.add(element);
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<Null> _refresh() async {
    _isUpdating = true;

    // waiting to load notification list
    //await new Future.delayed(new Duration(seconds: 2));
    try {
      int count = globalNotificationList.length;
      await globalWebService.getNotification();
      widget.mo.setNewNotification();

      if (globalNotificationList.length != count) {
        await globalWebService.getUserTask();
        await globalWebService.getSellerTask();
      }
    } catch (e) {
      print(e);
    }
    _isUpdating = false;
    updateListDisplayData();

    setState(() {});

    return null;
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScNotificationsSupplier]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScNotificationsSupplier]didChangeDependencies...');

    updateListDisplayData();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScNotificationsSupplier]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Scaffold(
      body: !globalUserData.isSeller()
          ? Container(
              padding: EdgeInsets.all(15),
              child: Center(
                  child: Text(
                'You are not subscripted as a Supplier.',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.black54,
                ),
              )),
            )
          : RefreshIndicator(
              key: _refreshKey,
              onRefresh: _refresh,
              child: ListView.builder(
                itemCount: listDisplayData.length,
                itemBuilder: (context, index) {
                  return InfoCardNotifications(
                    imgLink: listDisplayData[index].task_picture,
                    name: listDisplayData[index].task_title,
                    timeLog:
                        getTimeStampString(listDisplayData[index].time_stamp),
                    status: getStatus(listDisplayData[index].data_type),

                    //-- On Tap Process --
                    onTap: () async {
                      //-- delete and update notification --
                      try {
                        globalWebService.deleteNotification(
                            taskID: listDisplayData[index].id,
                            funcSuccess: () {
                              globalWebService.getNotification(funcSuccess: () {
                                widget.mo.setNewNotification();
                              });
                            });
                      } catch (e) {
                        print(e);
                      }

                      //-- [Enquiry] notification --
                      if (listDisplayData[index].data_type == 'enquiry') {
                        OpportunityData opportunityData;
                        globalSupplierEnquiresList.forEach((element) {
                          if (element.id == listDisplayData[index].task_id)
                            opportunityData = element;
                        });
                        if (opportunityData != null) {
                          await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScOpportunity(
                                      opportunityData: opportunityData,
                                    )),
                          );
                        }
                      } else if (listDisplayData[index].data_type ==
                          'declined') {
                        //-- [Declined] notification --
                        QuotedOpportunityData quotedOpportunityData;
                        globalSupplierQuotedList.forEach((element) {
                          if (element.id == listDisplayData[index].quote_id)
                            quotedOpportunityData = element;
                        });
                        if (quotedOpportunityData != null) {
                          await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScAcceptedSupplierJob(
                                      quotedOpportunityData:
                                          quotedOpportunityData,
                                    )),
                          );
                        }
                      } else {
                        //-- [accepted] or [finished] --
                        QuotedOpportunityData quotedOpportunityData;
                        globalSupplierAcceptedList.forEach((element) {
                          if (element.id == listDisplayData[index].quote_id)
                            quotedOpportunityData = element;
                        });
                        if (quotedOpportunityData != null) {
                          await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScAcceptedSupplierJob(
                                      quotedOpportunityData:
                                          quotedOpportunityData,
                                    )),
                          );
                        }
                      }

                      //-- come back and update the list --
                      //updateListDisplayData();
                      listDisplayData.removeAt(index);
                      setState(() {});
                    },
                  );
                },
              ),
            ),
    );
  }
}
