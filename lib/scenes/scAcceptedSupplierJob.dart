import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';

import '../global.dart';
import '../libWebData.dart';
import '../libMyLib.dart';
import '../objects/objInfoCard.dart';
import '../objects/objAppButton.dart';
import '../objects/objCacheImage.dart';
import '../objects/objTextDateFormat.dart';

class ScAcceptedSupplierJob extends StatefulWidget {
  final QuotedOpportunityData quotedOpportunityData;

  ScAcceptedSupplierJob({
    @required this.quotedOpportunityData,
  });

  @override
  _ScAcceptedSupplierJobState createState() => _ScAcceptedSupplierJobState();
}

class _ScAcceptedSupplierJobState extends State<ScAcceptedSupplierJob> {
  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  Future<void> _goDelete() async {
    String msg = widget.quotedOpportunityData.isAccepted == 0
        ? 'quote for'
        : 'finished task of';

    await libShowSelectionAlert(
        context, 'Delete $msg \"${widget.quotedOpportunityData.task.title}\"?',
        textNo: 'Cancel', textYes: 'Delete', funcYes: () async {
      setState(() {
        _waiting = true;
      });
      try {
        await globalWebService.deleteQuote(
            id: widget.quotedOpportunityData.id,
            funcSuccess: () async {
              // update user task list for the removed task.
              await globalWebService.getSellerTask();
              Navigator.of(context).pop();
            });
      } catch (e) {
        setState(() {
          _waiting = false;
        });
        String msg = e.toString();
        int idx = msg.indexOf(':') ?? -2;
        await libShowErrorAlert(context, msg.substring(idx + 2));
      }
    });
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScAcceptedSupplierJob]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScAcceptedSupplierJob]didChangeDependencies...');

    //widget.quotedOpportunityData.printContent();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScAcceptedSupplierJob]dispose...");
    super.dispose();
  }

  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Container(
          child: Scaffold(
            //-------------------
            //-------------------
            //-- Title App Bar --
            //-------------------
            appBar: AppBar(
              brightness: Brightness.light,
              backgroundColor: Colors.white,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              title: Text(
                widget.quotedOpportunityData.isAccepted == 0
                    ? 'Quoted Job'
                    : 'Accepted Job',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              centerTitle: true,
            ),
            body: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  //----------------------------------------
                  //----------------------------------------
                  // -- Middle scrolling view of contents --
                  //----------------------------------------
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          //--------------------------------------
                          //-- User avatar, name, phone & email --
                          //--------------------------------------
                          widget.quotedOpportunityData.isAccepted == 0
                              ? Container()
                              : Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        //-------------------------
                                        //-- Image and user name --
                                        Row(
                                          children: <Widget>[
                                            CacheAvatar(
                                                imageURL: widget
                                                    .quotedOpportunityData
                                                    .user
                                                    .picture),
                                            SizedBox(width: 10),
                                            Text(
                                              widget.quotedOpportunityData.user
                                                  .username,
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                        //---------------------
                                        //-- contact buttons --
                                        Row(
                                          children: <Widget>[
                                            CircleButton(
                                              iconData: Icons.phone,
                                              onTap: () async {
                                                if (_waiting) return;
                                                String url = 'tel:' +
                                                    widget.quotedOpportunityData
                                                        .user.phone;
                                                if (await canLaunch(url)) {
                                                  await launch(url);
                                                } else {
                                                  await libShowErrorAlert(
                                                      context,
                                                      'Error in calling the phone.');
                                                }
                                              },
                                            ),
                                            SizedBox(width: 10),
                                            CircleButton(
                                              iconData: Icons.email,
                                              onTap: () async {
                                                if (_waiting) return;
                                                String url = 'mailto:' +
                                                    widget.quotedOpportunityData
                                                        .user.email;
                                                if (await canLaunch(url)) {
                                                  await launch(url);
                                                } else {
                                                  await libShowErrorAlert(
                                                      context,
                                                      'Error in sending the mail.');
                                                }
                                              },
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Divider(color: Colors.grey),
                                  ],
                                ),
                          //---------------------
                          //-- Title and Price --
                          //---------------------
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      widget.quotedOpportunityData.task.title,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    //-----------------------
                                    //-- Date and Location --
                                    Row(
                                      children: <Widget>[
                                        Icon(Icons.calendar_today,
                                            color: GLOBAL_THEME_COLOR, size: 16),
                                        SizedBox(width: 5),
                                        TextDateFormat(
                                          dateString: widget.quotedOpportunityData
                                              .task.date_start,
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 5),
                                    //--------------
                                    //-- Location --
                                    Row(
                                      children: <Widget>[
                                        Icon(Icons.location_on,
                                            color: GLOBAL_THEME_COLOR, size: 16),
                                        SizedBox(width: 5),
                                        Expanded(
                                          child: Text(
                                            widget.quotedOpportunityData.task
                                                .address,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 15,
                                              color: GLOBAL_THEME_COLOR,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              //-----------------
                              //-- Quote Price --
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      'Quote price',
                                      style: TextStyle(
                                        fontSize: 14,
                                        //fontWeight: FontWeight.bold,
                                        color: Colors.grey,
                                      ),
                                    ),
                                    SizedBox(height: 2),
                                    Text(
                                      '\$${widget.quotedOpportunityData.quote_price}',
                                      style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold,
                                        color: GLOBAL_THEME_COLOR,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: 5),
                          Divider(color: Colors.grey),
                          //-----------------------
                          //-- Task Image --
                          //-----------------------
                          Text(
                            'Task Picture:',
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54,
                            ),
                          ),
                          SizedBox(height: 5),
                          CacheImage(
                              width: MediaQuery.of(context).size.width - 30,
                              height: (MediaQuery.of(context).size.width - 30) *
                                  0.7,
                              imageURL:
                                  widget.quotedOpportunityData.task.picture),
                          SizedBox(height: 15),
                          //-----------------------
                          //-- Quote Image --
                          //-----------------------
                          Text(
                            'Quotation Picture:',
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54,
                            ),
                          ),
                          SizedBox(height: 5),
                          CacheImage(
                              width: MediaQuery.of(context).size.width - 30,
                              height: (MediaQuery.of(context).size.width - 30) *
                                  0.7,
                              imageURL: widget.quotedOpportunityData.picture),
                          SizedBox(height: 15),
                          //----------------------
                          //-- Task Description --
                          //----------------------
                          Text(
                            'Task Description:',
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            widget.quotedOpportunityData.task.user_description,
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(height: 15),
                          //----------------------
                          //-- Seller Description --
                          //----------------------
                          Text(
                            'My Description:',
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            widget.quotedOpportunityData.seller_description,
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  //-------------------------------
                  //-------------------------------
                  //-- Accept and Reject buttons --
                  //-------------------------------
                  Container(
                    // delete button for either unaccepted quote or finished job
                    child: (widget.quotedOpportunityData.isAccepted == 0 ||
                            widget.quotedOpportunityData.task.task_finished ==
                                1)
                        ? AppButton(
                            //-- either delete a quote or a finished job
                            title: widget.quotedOpportunityData.isAccepted == 0
                                ? 'Delete quote'
                                : 'Delete finished job',
                            onPressed: () async {
                              await _goDelete();
                            },
                          )
                        : SizedBox(height: 1),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
