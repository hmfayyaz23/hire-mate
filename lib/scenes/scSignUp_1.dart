import 'package:flutter/material.dart';
import 'package:hire_mate_app/libWebData.dart';

import 'dart:io';
import 'dart:convert';

import '../global.dart';
import '../libMyLib.dart';
import '../libGetPicture.dart';
import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';

import 'scSignUp_2.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScSignUp1 extends StatefulWidget {
  @override
  _ScSignUp1State createState() => _ScSignUp1State();
}

class _ScSignUp1State extends State<ScSignUp1> {
//  String _inputEmail = 'tester004@email.com';
//  String _inputPassword = '123456';
  String _inputPassword = '';
  String _inputPassword2 = '';

  final FocusNode _focusEmail = FocusNode();
  final FocusNode _focusPhoneNumber = FocusNode();
  final FocusNode _focusPassword = FocusNode();
  final FocusNode _focusPassword2 = FocusNode();

  String _selectImageSource = '';
  File _image;

  //---------------------------------------------
  //---------------------------------------------
  Widget _bottomSheet(context) {
    return Container(
      child: Wrap(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.camera_alt),
              title: Text('Take a photo'),
              onTap: () {
                _selectImageSource = 'camera';
                Navigator.of(context).pop();
              }),
          ListTile(
            leading: Icon(Icons.image),
            title: Text('Image from gallery'),
            onTap: () {
              _selectImageSource = 'gallery';
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  //---------------------------------------------
  //---------------------------------------------
  // Get Image function
  //---------------------------------------------
  Future getImage() async {
    _selectImageSource = '';
    await showModalBottomSheet(context: context, builder: _bottomSheet);
    if (_selectImageSource.isEmpty) return;

    File savedImageFile = await getPicture(
      selectImageSource: _selectImageSource,
      isSquareImage: true,
    );
    if (savedImageFile == null) return;

    List<int> imageBytes = savedImageFile.readAsBytesSync();
    globalProfilePictureBase64 = base64Encode(imageBytes);
    //print(_imageBase64.length);

    setState(() {
      _image = savedImageFile;
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  //---------------------------------------------
  //-- Go Login Function --
  //---------------------------------------------
  Future<void> _goNext() async {
    if (globalProfileData.username.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a user name');
      return;
    }
    if (!libCheckValidEmail(globalProfileData.email)) {
      await libShowErrorAlert(context, 'Please enter valid email address');
      return;
    }
    if (globalProfileData.phone.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a phone number');
      return;
    }
    if (_inputPassword.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a password');
      return;
    }
    if (_inputPassword2.isEmpty) {
      await libShowErrorAlert(context, 'Please confirm password');
      return;
    }
    if (_inputPassword != _inputPassword2) {
      await libShowErrorAlert(context, 'Passwords entered not matched');
      return;
    }
    globalProfilePassword = _inputPassword;

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ScSignUp2()),
    );
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();

    // init profile data instance
    globalProfileData = UserData(
      username: '',
      email: '',
      phone: '',
      picture: false,
      location: '',
      location_lat: 0,
      location_lon: 0,
      roles: [],
      //-----
      abn: '',
      business_type: '',
      business_trading_name: '',
      service_type: 'Hire/Rental',
      category: [],
      service_locations: [],
    );
    globalProfilePictureBase64 = '';
    globalProfilePassword = '';
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() {
    super.didChangeDependencies();

    globalProfileData.printContent();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Scaffold(
      //-------------------
      //-- Title App Bar --
      //-------------------
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          'New account',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      //--------------------------
      //-- body of entry fields --
      //--------------------------
      body: Container(
        padding: const EdgeInsets.all(15.0),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      //---------------------
                      //-- Picture content --
                      //---------------------
                      InkWell(
                        onTap: () => getImage(),
                        child: SizedBox(
                          width: 120,
                          height: 120,
                          child: Stack(
                            children: <Widget>[
                              //-- picture --
                              (_image != null)
                                  ? CircleAvatar(
                                      backgroundImage: FileImage(_image),
                                      radius: 60,
                                      backgroundColor: Colors.white,
                                      foregroundColor: Colors.white,
                                    )
                                  : Container(
                                      width: 120.0,
                                      height: 120.0,
                                      decoration: BoxDecoration(
                                        color: Color(0x1fff0000),
                                        shape: BoxShape.circle,
                                      ),
                                      child: Icon(
                                        Icons.image,
                                        size: 50,
                                        color: GLOBAL_THEME_COLOR,
                                      ),
                                    ),
                              //-- edit icon --
                              Align(
                                alignment: Alignment(0, 1.4),
                                child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    color: GLOBAL_THEME_COLOR,
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.white, width: 2),
                                  ),
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                    size: 22.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      //--------------------
                      //-- name field --
                      //--------------------
                      EntryField(
                        label: 'Name',
                        onChanged: (text) {
                          globalProfileData.username = text;
                        },
                        onSubmitted: (value) {
                          FocusScope.of(context).requestFocus(_focusEmail);
                        },
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                      ),
                      //--------------------
                      //-- email field --
                      //--------------------
                      EntryField(
                        label: 'Email',
                        onChanged: (text) {
                          globalProfileData.email = text;
                        },
                        onSubmitted: (value) {
                          FocusScope.of(context)
                              .requestFocus(_focusPhoneNumber);
                        },
                        keyboardType: TextInputType.emailAddress,
                        focusNode: _focusEmail,
                        textInputAction: TextInputAction.next,
                      ),
                      //------------------------
                      //-- phone number field --
                      //------------------------
                      EntryField(
                        label: 'Phone',
                        onChanged: (text) {
                          globalProfileData.phone = text;
                        },
                        onSubmitted: (value) {
                          FocusScope.of(context).requestFocus(_focusPassword);
                        },
                        keyboardType: TextInputType.number,
                        focusNode: _focusPhoneNumber,
                        textInputAction: TextInputAction.next,
                      ),
                      //--------------------
                      //-- password field --
                      //--------------------
                      EntryField(
                        label: 'Password',
                        isObscureText: true,
                        onChanged: (text) {
                          _inputPassword = text;
                        },
                        onSubmitted: (value) {
                          FocusScope.of(context)
                              .requestFocus(_focusPassword2);
                        },
                        focusNode: _focusPassword,
                        textInputAction: TextInputAction.next,
                      ),
                      //--------------------
                      //-- password field --
                      //--------------------
                      EntryField(
                        label: 'Confirm Password',
                        isObscureText: true,
                        onChanged: (text) {
                          _inputPassword2 = text;
                        },
                        onSubmitted: (value) {
                          FocusScope.of(context).unfocus();
                        },
                        focusNode: _focusPassword2,
                        textInputAction: TextInputAction.done,
                      ),
                    ],
                  ),
                ),
              ),
              //------------------
              //-- Next button --
              //------------------
              AppButton(
                title: 'Next',
                onPressed: () async {
                  FocusScope.of(context).unfocus();
                  await _goNext();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
