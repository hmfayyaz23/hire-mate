import 'package:flutter/material.dart';

import '../global.dart';

//--------------------
//-- ListData Class --
//--------------------
class ListData {
  String title;
  bool isSelected;

  ListData({
    @required this.title,
    this.isSelected = false,
  });
}

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
class ScSelectServices extends StatefulWidget {
  final String choice;
  final List<String> selectedList;
  final bool isSingle;

  ScSelectServices({
    @required this.choice,
    this.selectedList = const [],
    this.isSingle = false,
  });
  @override
  _ScSelectServicesState createState() => _ScSelectServicesState();
}

class _ScSelectServicesState extends State<ScSelectServices> {
  List<ListData> listData = [];

  //-------------------------
  //-- Get result function --
  //-------------------------
  List<String> _getResult() {
    List<String> result = [];

    listData.forEach((element) {
      if (element.isSelected) {
        result.add(element.title);
      }
    });

    return result;
  }

  //-----------------------------------
  //-----------------------------------
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('[ScSelectServices]didChangeDependencies...');

    // Init the listed data
    List<String> selectedList = GLOBAL_SERVICES[widget.choice];
    selectedList.forEach((element) {
      listData.add(ListData(title: element));
    });

    // turn on any selected data
    if (widget.selectedList.isNotEmpty) {
      widget.selectedList.forEach((selectedElement) {
        listData.forEach((element) {
          if (element.title == selectedElement) element.isSelected = true;
        });
      });
    }
  }

  //-----------------------------------
  //-----------------------------------
  Widget build(BuildContext context) {
    return Scaffold(
      //-------------------
      //-- Title App Bar --
      //-------------------
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        centerTitle: true,
        //-- Back Button --
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        //-- Title --
        title: Text(
          widget.choice,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        //-- Save Button --
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: GLOBAL_THEME_COLOR,
              size: 30,
            ),
            onPressed: () {
              var result = _getResult();
              Navigator.of(context).pop(result);
            },
          ),
        ],
      ),
      //-- List of Selections --
      body: ListView.builder(
        itemCount: listData.length,
        itemBuilder: (context, index) {
          if (listData[index].title.contains('cat-----')) {
            //--------------
            //-- Category --
            //--------------
            return Container(
              height: 40,
              color: GLOBAL_THEME_COLOR,
              child: Center(
                child: Text(
                  listData[index].title.substring(8),
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            );
          } else {
            //---------------------
            //-- Selection tiles --
            // ---------------------
            return Column(
              children: <Widget>[
                ListTile(
                  title: Text(
                    listData[index].title,
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  trailing: Checkbox(
                    value: listData[index].isSelected,
                    activeColor: GLOBAL_THEME_COLOR,
                    onChanged: (value) {
                      setState(() {
                        if (widget.isSingle) {
                          listData.forEach((element) {
                            element.isSelected = false;
                          });
                        }
                        listData[index].isSelected = value;
                      });
                    },
                  ),
                ),
                Divider(color: GLOBAL_THEME_COLOR),
              ],
            );
          }
        },
      ),
    );
  }
}
