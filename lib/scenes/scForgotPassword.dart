import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

import '../global.dart';
import '../libMyLib.dart';

import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScForgotPassword extends StatefulWidget {
  @override
  _ScForgotPasswordState createState() => _ScForgotPasswordState();
}

class _ScForgotPasswordState extends State<ScForgotPassword> {
  String _inputEmail = '';
  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  //---------------------------------------------
  //-- Go Login Function --
  //---------------------------------------------
  Future<void> _goReset() async {
    await libShowSelectionAlert(context,
        'Instructions to reset your password will be sent to your registrated email address.',
        textNo: 'Cancel', textYes: 'Go reset', funcYes: () async {
      if (!libCheckValidEmail(_inputEmail)) {
        await libShowErrorAlert(context, 'Please enter valid email address');
        return;
      }

      setState(() {
        _waiting = true;
      });
      try {
        await globalWebService.resetPassword(
            email: _inputEmail,
            funcSuccess: () async {
              setState(() {
                _waiting = true;
              });
              await libShowErrorAlert(context,
                  'Success! Instructions to reset your password was sent to your registered email address.');
              Navigator.of(context).pop();
            });
      } catch (e) {
        setState(() {
          _waiting = false;
        });
        String msg = e.toString();
        int idx = msg.indexOf(':') ?? -2;
        await libShowErrorAlert(context, msg.substring(idx + 2));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => !_waiting,
      child: ProgressHUD(
        inAsyncCall: _waiting,
        child: Scaffold(
          //-------------------
          //-- Title App Bar --
          //-------------------
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            elevation: 0,
          ),
          //--------------------------
          //-- body of entry fields --
          //--------------------------
          body: Container(
            padding: const EdgeInsets.all(15.0),
            color: Colors.white,
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          //---------------------
                          //-- Picture content --
                          //---------------------
                          Container(
                            width: 150,
                            height: 150,
                            child: Stack(children: <Widget>[
                              CircleAvatar(
                                backgroundColor: GLOBAL_THEME_COLOR,
                                radius: 75,
                                child: Icon(
                                  Icons.lock,
                                  color: Colors.white,
                                  size: 100,
                                ),
                              ),
                              Align(
                                alignment: Alignment(.6, .6),
                                child: CircleAvatar(
                                  backgroundColor: GLOBAL_THEME_COLOR,
                                  radius: 20,
                                  child: Icon(
                                    Icons.help_outline,
                                    color: Colors.white,
                                    size: 40,
                                  ),
                                ),
                              ),
                            ]),
                          ),
                          SizedBox(height: 30),
                          //-----------
                          //-- Title --
                          //-----------
                          Text(
                            'Forgot your password?',
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 10),
                          //-------------
                          //-- Message --
                          //-------------
                          Text(
                            'Please enter the email address associated with your email. We will email you a link to reset your password.',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          SizedBox(height: 20),
                          //--------------------
                          //-- name field --
                          //--------------------
                          EntryField(
                            label: 'Email',
                            onChanged: (text) {
                              _inputEmail = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                          ),
                        ],
                      ),
                    ),
                  ),
                  //------------------
                  //-- Send button --
                  //------------------
                  AppButton(
                    title: 'Send',
                    onPressed: () async {
                      await _goReset();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void dispose() {
    super.dispose();
  }
}
