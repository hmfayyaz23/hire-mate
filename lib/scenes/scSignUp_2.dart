import 'package:flutter/material.dart';

import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../global.dart';
import '../libWebData.dart';
import '../libMyLib.dart';
import '../libMapTools.dart';

import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';

import 'scSelectServices.dart';
import 'scManageLocations.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScSignUp2 extends StatefulWidget {
  @override
  _ScSignUp2State createState() => _ScSignUp2State();
}

class _ScSignUp2State extends State<ScSignUp2> {
  bool _isCustomer;

  final TextEditingController _abnController = TextEditingController();
  final TextEditingController _businessTypeController = TextEditingController();
  final TextEditingController _businessTradingNameController =
      TextEditingController();
  final TextEditingController _addressController = TextEditingController();

  final FocusNode _focusBusinessType = FocusNode();
  final FocusNode _focusBusinessTradingName = FocusNode();

  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  Future<void> _goRegister(context) async {
    if (globalProfileData.location_lat == 0 ||
        globalProfileData.location_lon == 0) {
      try {
        LocationData result = await getLocation(globalProfileData.location);
        if (!result.isError) {
          globalProfileData.location_lat = result.lat;
          globalProfileData.location_lon = result.lng;
        } else {
          await libShowErrorAlert(context, 'Please enter a valid address.');
          return;
        }
      } catch (e) {
        print(e);
        await libShowErrorAlert(context, 'Please enter a valid address.');
        return;
      }
    }
    if (!_isCustomer && globalProfileData.abn.isEmpty) {
      await libShowErrorAlert(context, 'Please enter ABN.');
      return;
    }
    if (!_isCustomer && globalProfileData.business_trading_name.isEmpty) {
      await libShowErrorAlert(context, 'Please enter Business Trading Name.');
      return;
    }
    if (!_isCustomer && globalProfileData.service_locations.isEmpty) {
      await libShowErrorAlert(
          context, 'Please enter at least one service location.');
      return;
    }
    if (!_isCustomer && globalProfileData.category.isEmpty) {
      await libShowErrorAlert(
          context, 'Please select at least one service category.');
      return;
    }

    setState(() {
      _waiting = true;
    });
    try {
      await globalWebService.createUser(
          isSeller: !_isCustomer,
          funcSuccess: () async {
            await libShowErrorAlert(
                context, 'New user registered successfully');
            Navigator.popUntil(context, ModalRoute.withName('Login'));
          });
    } catch (e) {
      setState(() {
        _waiting = false;
      });
      String msg = e.toString();
      int idx = msg.indexOf(':') ?? -2;
      await libShowErrorAlert(context, msg.substring(idx + 2));
    }
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();

    _isCustomer = !globalProfileData.roles.contains('contractor');
    _addressController.text = globalProfileData.location;
    _abnController.text = globalProfileData.abn;
    _businessTypeController.text = globalProfileData.business_type;
    _businessTradingNameController.text =
        globalProfileData.business_trading_name;
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    _addressController.dispose();
    _abnController.dispose();
    _businessTypeController.dispose();
    _businessTradingNameController.dispose();

    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Scaffold(
          //-------------------
          //-- Title App Bar --
          //-------------------
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              'New account',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
          ),
          //--------------------------
          //-- body of entry fields --
          //--------------------------
          body: Container(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          //-- Account Type --
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Account Type',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                                //fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(height: 15),
                          //------------------------------
                          //-- Customer/Supplier button --
                          //------------------------------
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 40,
                                  //---------------------
                                  //-- customer button --
                                  //---------------------
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        side: BorderSide(
                                            color: GLOBAL_THEME_COLOR)),
                                    onPressed: () {
                                      if (!_isCustomer) {
                                        globalProfileData.roles = ['customer'];
                                        setState(() {
                                          _isCustomer = true;
                                        });
                                      }
                                    },
                                    color: _isCustomer
                                        ? GLOBAL_THEME_COLOR
                                        : Colors.white,
                                    textColor: _isCustomer
                                        ? Colors.white
                                        : GLOBAL_THEME_COLOR,
                                    child: Text('Customer',
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        )),
                                  ),
                                ),
                              ),
                              SizedBox(width: 15),
                              Expanded(
                                child: Container(
                                  height: 40,
                                  //-- supplier button --
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        side: BorderSide(
                                            color: GLOBAL_THEME_COLOR)),
                                    onPressed: () {
                                      if (_isCustomer) {
                                        globalProfileData.roles = [
                                          'customer',
                                          'contractor'
                                        ];
                                        setState(() {
                                          _isCustomer = false;
                                        });
                                      }
                                    },
                                    color: _isCustomer
                                        ? Colors.white
                                        : GLOBAL_THEME_COLOR,
                                    textColor: _isCustomer
                                        ? GLOBAL_THEME_COLOR
                                        : Colors.white,
                                    child: Text('Supplier',
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          //-----------------------
                          //-- Address TextField --
                          //-----------------------
                          EntryField(
                            readOnly: true,
                            onTap: () {
                              double lat = (globalProfileData.location_lat == 0)
                                  ? GLOBAL_INIT_LAT
                                  : globalProfileData.location_lat;
                              double lng = (globalProfileData.location_lon == 0)
                                  ? GLOBAL_INIT_LNG
                                  : globalProfileData.location_lon;
                              bool isToMyLocation =
                                  (globalProfileData.location_lat == 0 &&
                                      globalProfileData.location_lon == 0);
                              setState(() async {
                                LocationResult result =
                                    await showLocationPicker(
                                  context,
                                  GLOBAL_MAP_KEY,
                                  initialCenter: LatLng(lat, lng),
                                  automaticallyAnimateToCurrentLocation:
                                      isToMyLocation,
                                  myLocationButtonEnabled: false,
                                  requiredGPS: false,
                                  layersButtonEnabled: false,
                                  appBarColor: Colors.black26,
                                );
                                _addressController.text = result.address;
                                globalProfileData.location = result.address;
                                globalProfileData.location_lat =
                                    result.latLng.latitude;
                                globalProfileData.location_lon =
                                    result.latLng.longitude;
                              });
                            },
                            label: 'Address',
//                            onChanged: (text) {
//                              globalProfileData.location = text;
//                              globalProfileData.location_lat = 0;
//                              globalProfileData.location_lon = 0;
//                            },
//                            onSubmitted: (value) {
//                              FocusScope.of(context).unfocus();
//                            },
                            controller: _addressController,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                          ),
                          //----------------------
                          //-- Locate Me Button --
                          //----------------------
//                          Align(
//                            alignment: Alignment.centerRight,
//                            child: FlatButton(
//                              onPressed: () {
//                                setState(() async {
//                                  LocationResult result =
//                                      await showLocationPicker(
//                                    context,
//                                    GLOBAL_MAP_KEY,
//                                    initialCenter: LatLng(
//                                        GLOBAL_INIT_LAT, GLOBAL_INIT_LNG),
//                                    automaticallyAnimateToCurrentLocation:
//                                        false,
//                                    myLocationButtonEnabled: false,
//                                    requiredGPS: false,
//                                    layersButtonEnabled: false,
//                                    appBarColor: Colors.black26,
//                                  );
//                                  _addressController.text = result.address;
//                                  globalProfileData.location = result.address;
//                                  globalProfileData.location_lat =
//                                      result.latLng.latitude;
//                                  globalProfileData.location_lon =
//                                      result.latLng.longitude;
//                                });
//                              },
//                              child: Text(
//                                'Locate me',
//                                style: TextStyle(
//                                  color: GLOBAL_THEME_COLOR,
//                                  fontSize: 16,
//                                  decoration: TextDecoration.underline,
//                                ),
//                              ),
//                            ),
//                          ),
                          //--------------------
                          //--------------------
                          //--------------------
                          //-- Supplier Items --
                          //--------------------
                          Visibility(
                            visible: !_isCustomer,
                            child: Column(
                              children: <Widget>[
                                //----------------------
                                //-- ABN --
                                //----------------------
                                EntryField(
                                  label: 'ABN',
                                  isDense: true,
                                  onChanged: (text) {
                                    globalProfileData.abn = text;
                                  },
                                  onSubmitted: (value) {
                                    FocusScope.of(context)
                                        .requestFocus(_focusBusinessType);
                                  },
                                  controller: _abnController,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.next,
                                ),
                                //-------------------
                                //-- Business Type --
                                //-------------------
                                EntryField(
                                  label: 'Business Type (Optional)',
                                  isDense: true,
                                  onChanged: (text) {
                                    globalProfileData.business_type = text;
                                  },
                                  onSubmitted: (value) {
                                    FocusScope.of(context).requestFocus(
                                        _focusBusinessTradingName);
                                  },
                                  controller: _businessTypeController,
                                  focusNode: _focusBusinessType,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.next,
                                ),
                                //---------------------------
                                //-- Business Trading Name --
                                //---------------------------
                                EntryField(
                                  label: 'Business Trading Name',
                                  isDense: true,
                                  onChanged: (text) {
                                    globalProfileData.business_trading_name =
                                        text;
                                  },
                                  onSubmitted: (value) {
                                    FocusScope.of(context).unfocus();
                                  },
                                  controller: _businessTradingNameController,
                                  focusNode: _focusBusinessTradingName,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.done,
                                ),
                                SizedBox(height: 15),
                                //----------------------
                                //-- Service location --
                                //----------------------
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Service Locations',
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 20,
                                      //fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                                //------------------------------
                                //-- Manage service locations --
                                //------------------------------
                                NextPageButton(
                                  title: 'Manage service location',
                                  icon: Icons.location_on,
                                  onTap: () async {
                                    var list = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ScManageLocations(
                                                listServiceLocation:
                                                    globalProfileData
                                                        .service_locations,
                                              )),
                                    );

                                    // replace new saved data
                                    if (list is List<ServiceLocation>)
                                      globalProfileData.service_locations =
                                          list;
                                  },
                                ),
                                SizedBox(height: 10),
                                //------------------
                                //------------------
                                //-- Service type --
                                //------------------
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Service Type',
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 20,
                                      //fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                                //-----------------
                                //-- Hire/Rental --
                                //-----------------
                                NextPageButton(
                                  title: 'Hire/Rental',
                                  color: Colors.black,
                                  icon: Icons.work,
                                  count:
                                      '${globalProfileData.category.length}/${GLOBAL_SERVICES['Hire/Rental'].length - 4}',
                                  onTap: () async {
                                    var result = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ScSelectServices(
                                                choice: 'Hire/Rental',
                                                selectedList:
                                                    globalProfileData.category,
                                              )),
                                    );
                                    if (result is List<String>) {
                                      setState(() {
                                        globalProfileData.category = result;
                                      });
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  //---------------------
                  //-- Register button --
                  //---------------------
                  AppButton(
                    title: 'Register',
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      await _goRegister(context);
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
