import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../global.dart';
import '../libWebData.dart';
import '../libMyLib.dart';
import '../objects/objInfoCard.dart';
import '../objects/objCacheImage.dart';
import '../objects/objTextDateFormat.dart';

import 'scQuote.dart';

class ScDetails extends StatefulWidget {
  final TaskData taskData;

  ScDetails({
    @required this.taskData,
  });

  @override
  _ScDetailsState createState() => _ScDetailsState();
}

class _ScDetailsState extends State<ScDetails> {
  bool _waiting = false;
  List<QuoteData> _availableQuoteList;

  //---------------------------------------------
  //---------------------------------------------
  void _setAvailableQuoteList() {
    _availableQuoteList = [];
    globalUserQuotedList.forEach((element) {
      // remove any accepted/declined quotes
      if (element.task_id == widget.taskData.id &&
          element.isDeclined == 0 &&
          element.isAccepted == 0) {
        _availableQuoteList.add(element);
      }
    });
  }

  //---------------------------------------------
  // Delete task function
  //---------------------------------------------
  Future<void> _goDeleteTask() async {
    await libShowSelectionAlert(
        context, 'Delete task \"${widget.taskData.title}\"?',
        textNo: 'Cancel', textYes: 'Delete', funcYes: () async {
      setState(() {
        _waiting = true;
      });
      try {
        await globalWebService.deleteUserTask(
            taskID: widget.taskData.id,
            funcSuccess: () async {
              // update user task list for the removed task.
              await globalWebService.getUserTask();
              Navigator.of(context).pop();
            });
      } catch (e) {
        setState(() {
          _waiting = false;
        });
        String msg = e.toString();
        int idx = msg.indexOf(':') ?? -2;
        await libShowErrorAlert(context, msg.substring(idx + 2));
      }
    });
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[scDetails]initState...');

    _setAvailableQuoteList();
    //widget.taskData.printContent();
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[scDetails]didChangeDependencies...');

  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[scDetails]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Scaffold(
            //-------------------
            //-- Title App Bar --
            //-------------------
            appBar: AppBar(
              brightness: Brightness.light,
              backgroundColor: Colors.white,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              title: Text(
                'Details',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //-------------------------------
                  //-- Title, Location and Price --
                  //-------------------------------
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          //-- Title --
                          Text(
                            widget.taskData.title,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 5),
                          //-- Location --
                          Row(
                            children: <Widget>[
                              Icon(Icons.location_on,
                                  color: GLOBAL_THEME_COLOR, size: 16),
                              Container(
                                width: 220,
                                child: Text(
                                  widget.taskData.address,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: GLOBAL_THEME_COLOR,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      //-- Price --
                      Text(
                        (widget.taskData.budget_price == null) ? 'Negotiate' : '\$${widget.taskData.budget_price}',
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: GLOBAL_THEME_COLOR,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  //-----------------------
                  //-- Image --
                  //-----------------------
                  CacheImage(
                    width: MediaQuery.of(context).size.width - 30,
                    height: (MediaQuery.of(context).size.width - 30)* 0.75,
                    imageURL: widget.taskData.picture,
                  ),
                  //----------------------------
                  //-- Date and Delete button --
                  //----------------------------
                  Container(
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        TextDateFormat(
                            dateString: widget.taskData.date_start),
                        IconButton(
                          iconSize: 25,
                          icon: Icon(Icons.delete_outline, color: GLOBAL_THEME_COLOR),
                          onPressed: () async {
                            await _goDeleteTask();
                          },
                        ),
                      ],
                    ),
                  ),
                  //----------------------
                  //-- Task Description --
                  //----------------------
                  Text(
                    widget.taskData.user_description,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.grey,
                    ),
                  ),
                  //---------------------------
                  //-- Bar: Available quotes --
                  //---------------------------
                  Container(
                    width: double.infinity,
                    height: 40,
                    margin: EdgeInsets.only(top: 15),
                    padding: EdgeInsets.only(left: 10),
                    color: Colors.grey[100],
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.assignment, color: GLOBAL_THEME_COLOR),
                        SizedBox(width: 10),
                        Text(
                          'Available quotes',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  //------------------------------
                  //-- List view of quotations ---
                  //------------------------------
                  ListView.builder(
                    primary: false, //required inside SingleChildScrollView
                    shrinkWrap: true, //required inside SingleChildScrollView
                    itemCount: _availableQuoteList.length,
                    itemBuilder: (context, index) {
                      return InfoCardQuote(
                        onTap: () async {
                          await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScQuote(
                                      quoteData: _availableQuoteList[index],
                                    )),
                          );
                          setState(() {
                            _setAvailableQuoteList();
                          }); // quote list may be modified (declined/accepted)
                        },
                        imgLink: _availableQuoteList[index].seller.picture,
                        name: _availableQuoteList[index].seller.username,
                        price: '\$${_availableQuoteList[index].quote_price}',
                        description:
                            _availableQuoteList[index].seller_description,
                      );
                    },
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
