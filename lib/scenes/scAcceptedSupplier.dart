import 'package:flutter/material.dart';

import '../global.dart';

import 'scAcceptedSupplierQuoted.dart';
import 'scAcceptedSupplierAccepted.dart';

class ScAcceptedSupplier extends StatefulWidget {
  @override
  _ScAcceptedSupplierState createState() => _ScAcceptedSupplierState();
}

class _ScAcceptedSupplierState extends State<ScAcceptedSupplier> {
  bool _isQuotedTask = (globalAcceptedSupplierIndex == 0);

  PageController pageController = PageController(
    initialPage: globalAcceptedSupplierIndex,
    keepPage: true,
  );

  //----------------------------------
  //-- Function to switch user role --
  //----------------------------------
  void _switchTaskList() {
    setState(() {
      globalAcceptedSupplierIndex = _isQuotedTask ? 0 : 1;
      pageController.jumpToPage(globalAcceptedSupplierIndex);
    });
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScAcceptedSupplier]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScAcceptedSupplier]didChangeDependencies...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScAcceptedSupplier]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return !globalUserData.isSeller()
        ? Container(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),
            child: Center(
                child: Text(
              'You are not subscripted as a Supplier.',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.black54,
              ),
            )),
          )
        : Container(
            margin: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      //------------------------
                      //-- Quoted Jobs button --
                      //------------------------
                      FlatButton(
                        onPressed: () {
                          if (!_isQuotedTask) {
                            _isQuotedTask = true;
                            _switchTaskList();
                          }
                        },
                        child: Text(
                          'Quoted Jobs',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: _isQuotedTask ? FontWeight.bold : FontWeight.normal,
                            color: _isQuotedTask ? Colors.black : Colors.black26,
                          ),
                        ),
                      ),
                      VerticalDivider(color: Colors.grey),
                      //--------------------------
                      //-- Accepted Jobs button --
                      //--------------------------
                      FlatButton(
                        onPressed: () {
                          if (_isQuotedTask) {
                            _isQuotedTask = false;
                            _switchTaskList();
                          }
                        },
                        child: Text(
                          'Accepted Jobs',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: _isQuotedTask ? FontWeight.normal : FontWeight.bold,
                            color: _isQuotedTask ? Colors.black26 : Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                //---------------------------------------------
                //---------------------------------------------
                //---------------------------------------------
                //-- Body --
                //---------------------------------------------
                Expanded(
                  child: PageView(
                    controller: pageController,
                    physics: NeverScrollableScrollPhysics(), //disable swipe
                    onPageChanged: (index) {
                      print('page changed');
                      //setState(() {});
                    },
                    children: <Widget>[
                      ScAcceptedSupplierQuoted(),
                      ScAcceptedSupplierAccepted(),
                    ],
                  ),
                )
              ],
            ),
          );
  }
}
