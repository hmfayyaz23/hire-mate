import 'package:flutter/material.dart';

import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hire_mate_app/libWebData.dart';

import 'package:intl/intl.dart';

import '../global.dart';
import '../libMyLib.dart';
import '../libMapTools.dart';
import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';
import 'package:flutter/foundation.dart';
import 'scSelectServices.dart';
import 'scNewJob_2.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScNewJob1 extends StatefulWidget {
  @override
  _ScNewJob1State createState() => _ScNewJob1State();
}

class _ScNewJob1State extends State<ScNewJob1> {
  String _service = 'Hire/Rental';
  List<String> _category = [];
  DateTime _date = DateTime(2000);
  String _inputLocation = '';
  double _inputLatitude = 0;
  double _inputLongitude = 0;
  String _inputBudget = '';
  String _displayServiceType = 'Select service type';

  final FocusNode _focusBudget = FocusNode();

  final TextEditingController _locationController = TextEditingController();
  final TextEditingController _budgetController = TextEditingController();

  Future<void> _goNext() async {
    if (_category.isEmpty) {
      await libShowErrorAlert(
          context, 'Please select service type.');
      return;
    }
    if (_inputLocation.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a location.');
      return;
    }

    if (_date.year == 2000) {
      await libShowErrorAlert(context, 'Please pick a date.');
      return;
    }

    if (_inputLatitude == 0 || _inputLongitude == 0) {
      try {
        LocationData result = await getLocation(_inputLocation);
        if (!result.isError) {
          _inputLatitude = result.lat;
          _inputLongitude = result.lng;
        } else {
          await libShowErrorAlert(context, 'Please enter a valid location.');
          return;
        }
      } catch (e) {
        print(e);
        await libShowErrorAlert(context, 'Please enter a valid location.');
        return;
      }
    }

    globalNewJobData.service_type = _service;
    globalNewJobData.category = _category;
    globalNewJobData.date_start = DateFormat('dd/MM/yy').format(_date);
    globalNewJobData.address = _inputLocation;
    globalNewJobData.latitude = _inputLatitude;
    globalNewJobData.longitude = _inputLongitude;
    globalNewJobData.budget_price = _inputBudget;

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ScNewJob2()),
    );
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScNewJob1]initState...');

    //Init new job entry data
    globalNewJobData = TaskData();
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScNewJob1]didChangeDependencies...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    _locationController.dispose();
    _budgetController.dispose();

    print("[ScNewJob1]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Scaffold(
      //-------------------
      //-- Title App Bar --
      //-------------------
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          'New job',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      //--------------------------
      //-- body of entry fields --
      //--------------------------
      body: Container(
        padding: const EdgeInsets.all(15.0),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //------------------
                      //------------------
                      //-- Service type --
                      //------------------
                      Row(
                        children: <Widget>[
                          Text(
                            'Service Type', // category previously
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontSize: 20,
                              color: GLOBAL_THEME_COLOR,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      NextPageButton(
                        title: _displayServiceType,
                        icon: Icons.work,
                        color: Colors.black,
                        onTap: () async {
                          var result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScSelectServices(
                                      choice: 'Hire/Rental',
                                      isSingle: true,
                                      selectedList: _category,
                                    )),
                          );
                          if (result is List<String>) {
                            setState(() {
                              // only one will be selected
                              _category = result;
                              _displayServiceType = result[0];
                            });
                          }
                        },
                      ),
                      Divider(color: GLOBAL_THEME_COLOR, thickness: 1),
                      SizedBox(height: 20),
                      //--------------------
                      //--------------------
                      //-- Location field --
                      //--------------------
                      Row(
                        children: <Widget>[
                          Text(
                            'Location', // category previously
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontSize: 20,
                              color: GLOBAL_THEME_COLOR,
                            ),
                          ),
                        ],
                      ),
                      EntryField(
                        readOnly: true,
                        onTap: () {
                          double lat = (_inputLatitude  == 0) ? GLOBAL_INIT_LAT : _inputLatitude;
                          double lng = (_inputLongitude == 0) ? GLOBAL_INIT_LNG : _inputLongitude;
                          bool isToMyLocation = (_inputLatitude  == 0 && _inputLongitude == 0);
                          setState(() async {
                            LocationResult result = await showLocationPicker(
                              context,
                              GLOBAL_MAP_KEY,
                              initialCenter: LatLng(lat, lng),
                              automaticallyAnimateToCurrentLocation: isToMyLocation,
                              myLocationButtonEnabled: false,
                              requiredGPS: false,
                              layersButtonEnabled: false,
                              appBarColor: Colors.black26,
                            );

                              String resultAddress = result.address;

                              _inputLocation = resultAddress;
                              _locationController.text = resultAddress;

                              _inputLatitude = result.latLng.latitude;
                              _inputLongitude = result.latLng.longitude;
                            
                          });
                        },
                      //  onChanged: (text) {
                      //    _inputLocation = text;
                      //    _inputLatitude = 0;
                      //    _inputLongitude = 0;
                      //  },
                      //  onSubmitted: (value) {
                      //    FocusScope.of(context).requestFocus(_focusBudget);
                      //  },
                        controller: _locationController,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                      ),
                      //----------------------
                      //----------------------
                      //-- Locate Me Button --
                      //----------------------
                      SizedBox(height: 30),
//                      Align(
//                        alignment: Alignment.centerRight,
//                        child: FlatButton(
//                          onPressed: () {
//                            setState(() async {
//                              LocationResult result = await showLocationPicker(
//                                context,
//                                GLOBAL_MAP_KEY,
//                                initialCenter: LatLng(GLOBAL_INIT_LAT, GLOBAL_INIT_LNG),
//                                automaticallyAnimateToCurrentLocation: false,
//                                myLocationButtonEnabled: false,
//                                requiredGPS: false,
//                                layersButtonEnabled: false,
//                                appBarColor: Colors.black26,
//                              );
//                              _locationController.text = result.address;
//                              _inputLocation = result.address;
//                              _inputLatitude = result.latLng.latitude;
//                              _inputLongitude = result.latLng.longitude;
//                            });
//                          },
//                          child: Text(
//                            'Locate me',
//                            style: TextStyle(
//                              color: GLOBAL_THEME_COLOR,
//                              fontSize: 16,
//                              decoration: TextDecoration.underline,
//                            ),
//                          ),
//                        ),
//                      ),
                      //----------
                      //----------
                      //-- Date --
                      //----------
                      Row(
                        children: <Widget>[
                          Text(
                            'Date', // category previously
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontSize: 20,
                              color: GLOBAL_THEME_COLOR,
                            ),
                          ),
                        ],
                      ),
                      ListTile(
                        onTap: () async {
                          var result = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now().add(Duration(days: 1)),
                            firstDate: DateTime.now(),
                            lastDate: DateTime.now().add(Duration(days: 3650)),
                          );
                          if (result != null) {
                            setState(() {
                              _date = result;
                            });
                          }
                        },
                        leading: Text(
                          (_date.year == 2000)
                              ? '<Pick a date>'
                              : '${DateFormat.yMMMd().format(_date)}',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        trailing: Icon(
                          Icons.calendar_today,
                          size: 18,
                          color: GLOBAL_THEME_COLOR,
                        ),
                      ),
                      //Divider(color: GLOBAL_THEME_COLOR),
                      //--------------------
                      //--------------------
                      //-- Budget field --
                      //--------------------
                      EntryField(
                        label: 'Budget (optional)',
                        prefix: '\$',
                        onChanged: (text) {
                          _inputBudget = text;
                        },
                        onSubmitted: (value) {
                          FocusScope.of(context).unfocus();
                        },
                        controller: _budgetController,
                        focusNode: _focusBudget,
                        textInputAction: TextInputAction.done,
                      ),
                    ],
                  ),
                ),
              ),
              //------------------
              //------------------
              //-- Next button --
              //------------------
              AppButton(
                title: 'Next',
                onPressed: () async {
                  FocusScope.of(context).unfocus();
                  await _goNext();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//-----------------------------------------------
//-----------------------------------------------
//-- Upper Button Widget Class --
//-----------------------------------------------
class ServiceTypeButton extends StatelessWidget {
  final bool isSelected;
  final String title;
  final IconData iconData;
  final Function onTap;

  const ServiceTypeButton({
    Key key,
    @required this.isSelected,
    @required this.title,
    @required this.iconData,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 100,
        child: Column(
          children: <Widget>[
            CircleAvatar(
              radius: 25,
              backgroundColor: GLOBAL_THEME_COLOR.withOpacity(0.5),
              child: CircleAvatar(
                radius: 24,
                backgroundColor: isSelected ? GLOBAL_THEME_COLOR : Colors.white,
                child: Icon(
                  iconData,
                  color: isSelected
                      ? Colors.white
                      : GLOBAL_THEME_COLOR.withOpacity(0.5),
                  size: 25,
                ),
              ),
            ),
            SizedBox(height: 5),
            Text(
              title,
              style: TextStyle(
                fontSize: 14,
                color: isSelected
                    ? GLOBAL_THEME_COLOR
                    : GLOBAL_THEME_COLOR.withOpacity(0.5),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
