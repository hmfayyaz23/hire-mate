import 'package:flutter/material.dart';

import '../global.dart';
import '../libWebData.dart';
import '../objects/objInfoCard.dart';
import '../objects/objTextDateFormat.dart';

import 'scDetails.dart';
import 'scNewJob_1.dart';

class ScMeCustomer extends StatefulWidget {
  final double startPosition;
  ScMeCustomer({
    this.startPosition = 0,
  });

  @override
  _ScMeCustomerState createState() => _ScMeCustomerState();
}

class _ScMeCustomerState extends State<ScMeCustomer> {
  // RefreshIndicator requires key to implement state (for RefreshIndicator)
  final GlobalKey<RefreshIndicatorState> _refreshKey =
      GlobalKey<RefreshIndicatorState>();
  // For scrolling control
  ScrollController _scrollController;
  double _offset;

  List<TaskData> _listTaskData;

  bool _isUpdating = false;

  //---------------------------------------------
  //---------------------------------------------
  void _updateEnquiriesList() {
    _listTaskData = [];

    globalUserEnquiresList.forEach((element) {
      //print('---------------------');
      //element.printContent();
      if (element.task_finished == 2) {
        //-- delete orphan task here... --
        // a finish task with quotation deleted => task_finished == 2
        print('Remove orphan task -> ${element.title}');
        globalWebService.deleteUserTask(taskID: element.id);
      } else if (element.task_finished == 0) {
        //-- for all un-finished tasks --
        int taskID = element.id;
        bool isAccepted = false;
        // check if task accepted
        globalUserAcceptedList.forEach((element) {
          if (element.task_id == taskID) isAccepted = true;
        });

        if (!isAccepted) _listTaskData.add(element);
      }
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  //-- Pull Down Refresh Function --
  //---------------------------------------------
  Future<Null> _refresh() async {
    _isUpdating = true;
    try {
      await globalWebService.getUserTask();
    } catch (e) {
      print(e);
    }
    _isUpdating = false;
    _updateEnquiriesList();

    setState(() {});

    return null;
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[scMeCustomer]initState...');

    // Init ListView scroller control
    _offset = widget.startPosition;
    _scrollController =
        ScrollController(initialScrollOffset: widget.startPosition);
    _scrollController.addListener(() {
      _offset = _scrollController.offset;
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[scMeCustomer]didChangeDependencies...');

    _updateEnquiriesList();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    globalOffsetScMeCustomer = _offset;

    print("[scMeCustomer]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(left: 10.0, right: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //-------------
            //-- My jobs --
            //-------------
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text(
                'My jobs',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
            ),
            //---------------
            //-- List View --
            //---------------
            Expanded(
              child: RefreshIndicator(
                key: _refreshKey,
                onRefresh: _refresh,
                child: ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  controller: _scrollController,
                  itemCount: _listTaskData.length,
                  itemBuilder: (context, index) {
                    TaskData taskData = _listTaskData[index];
                    int taskID = taskData.id;
                    int count = 0;

                    // check no. of quotes
                    globalUserQuotedList.forEach((element) {
                      if (element.task_id == taskID &&
                          element.isDeclined == 0) {
                        count++;
                      }
                    });

                    return InfoCard(
                      imgLink: taskData.picture,
                      title: taskData.title,
                      price: '\$${taskData.budget_price}',
                      location: taskData.address,
                      startDate: getFormattedString(taskData.date_start),
                      subtitle: (count == 9999)
                          ? 'Accepted'
                          : ((count == 0)
                              ? 'no quotation available yet'
                              : '$count quoation available'),
                      onTap: () async {
                        if (!_isUpdating) {
                          await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScDetails(
                                      taskData: taskData,
                                    )),
                          );
                          setState(() {
                            _updateEnquiriesList(); // task may be deleted
                          });
                        }
                      },
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
      //---------------------------------------------
      //-- Floating button --
      //---------------------------------------------
      floatingActionButton: SizedBox(
        width: 75,
        height: 75,
        child: FloatingActionButton(
          backgroundColor: GLOBAL_THEME_COLOR,
          onPressed: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ScNewJob1()),
            );
            //change state,  New task maybe added.
            setState(() {
              _updateEnquiriesList();
            });
          },
          child: Icon(Icons.add, size: 40),
        ),
      ),
    );
  }
}
