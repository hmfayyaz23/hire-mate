import 'package:flutter/material.dart';
import 'package:hire_mate_app/scenes/scAcceptedCustomerQuoted.dart';

import '../global.dart';

import 'scAcceptedCustomerQuoted.dart';
import 'scAcceptedCustomerAccepted.dart';

class ScAcceptedCustomer extends StatefulWidget {
  @override
  _ScAcceptedCustomerState createState() => _ScAcceptedCustomerState();
}

class _ScAcceptedCustomerState extends State<ScAcceptedCustomer> {
  bool _isQuotedTask = (globalAcceptedCustomerIndex == 0);

  PageController pageController = PageController(
    initialPage: globalAcceptedCustomerIndex,
    keepPage: true,
  );

  //----------------------------------
  //-- Function to switch user role --
  //----------------------------------
  void _switchTaskList() {
    setState(() {
      globalAcceptedCustomerIndex = _isQuotedTask ? 0 : 1;
      pageController.jumpToPage(globalAcceptedCustomerIndex);
    });
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScAcceptedCustomer]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScAcceptedCustomer]didChangeDependencies...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScAcceptedCustomer]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 20,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                //------------------------
                //-- Quoted Jobs button --
                //------------------------
                FlatButton(
                  onPressed: () {
                    if (!_isQuotedTask) {
                      _isQuotedTask = true;
                      _switchTaskList();
                    }
                  },
                  child: Text(
                    'Quoted Jobs',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: _isQuotedTask ? FontWeight.bold : FontWeight.normal,
                      color: _isQuotedTask ? Colors.black : Colors.black26,
                    ),
                  ),
                ),
                VerticalDivider(color: Colors.grey),
                //--------------------------
                //-- Accepted Jobs button --
                //--------------------------
                FlatButton(
                  onPressed: () {
                    if (_isQuotedTask) {
                      _isQuotedTask = false;
                      _switchTaskList();
                    }
                  },
                  child: Text(
                    'Accepted Jobs',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: _isQuotedTask ? FontWeight.normal : FontWeight.bold,
                      color: _isQuotedTask ? Colors.black26 : Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          //---------------------------------------------
          //---------------------------------------------
          //---------------------------------------------
          //-- Body --
          //---------------------------------------------
          Expanded(
            child: PageView(
              controller: pageController,
              physics: NeverScrollableScrollPhysics(), //disable swipe
              onPageChanged: (index) {
                print('page changed');
                //setState(() {});
              },
              children: <Widget>[
                ScAcceptedCustomerQuoted(),
                ScAcceptedCustomerAccepted(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
