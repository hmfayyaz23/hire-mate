import 'package:flutter/material.dart';
import 'package:hire_mate_app/scenes/scAcceptedSupplierJob.dart';

import '../global.dart';
import '../libWebData.dart';
import '../objects/objInfoCard.dart';
import '../objects/objTextDateFormat.dart';

class ScAcceptedSupplierAccepted extends StatefulWidget {
  @override
  _ScAcceptedSupplierAcceptedState createState() => _ScAcceptedSupplierAcceptedState();
}

class _ScAcceptedSupplierAcceptedState extends State<ScAcceptedSupplierAccepted> {
  // RefreshIndicator requires key to implement state (for RefreshIndicator)
  final GlobalKey<RefreshIndicatorState> _refreshKey = GlobalKey<RefreshIndicatorState>();
  // For scrolling control
  ScrollController _scrollController;

  List<QuotedOpportunityData> _listOpportunityData = [];

  bool _isUpdating = false;

  //---------------------------------------------
  //---------------------------------------------
  void _setupQuotedList() {
    _listOpportunityData = globalSupplierAcceptedList;
  }

  //---------------------------------------------
  //---------------------------------------------
  String _getSubtitle(index) {
    String result;

    if (_listOpportunityData[index].task.task_finished == 1)
      result = 'Job finished';
    else if (_listOpportunityData[index].isNewAccepted == 1)
      result = 'New accepted';
    else
      result = 'In progress';

    return result;
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<Null> _refresh() async {
    _isUpdating = true;
    await globalWebService.getSellerTask(funcSuccess: () {
      _setupQuotedList();
      _isUpdating = false;
    });

    setState(() {});

    return null;
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScAcceptedSupplierAccepted]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScAcceptedSupplierAccepted]didChangeDependencies...');

    _setupQuotedList();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScAcceptedSupplierAccepted]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Container(
      child: RefreshIndicator(
        key: _refreshKey,
        onRefresh: _refresh,
        child: ListView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          controller: _scrollController,
          itemCount: _listOpportunityData.length,
          itemBuilder: (context, index) {
            return InfoCard(
              imgLink: _listOpportunityData[index].picture,
              title: _listOpportunityData[index].task.title,
              price: '\$${_listOpportunityData[index].quote_price}',
              location: _listOpportunityData[index].task.address,
              startDate: getFormattedString(_listOpportunityData[index].task.date_start),
              subtitle: _getSubtitle(index),
              onTap: () async {
                // expire new accepted quote
                if (_listOpportunityData[index].isNewAccepted == 1) {
                  globalWebService.updateQuote(
                      id: _listOpportunityData[index].id,
                      isNewAccepted: 0,
                      funcSuccess: () {
                        _listOpportunityData[index].isNewAccepted = 0;
                      });
                }
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ScAcceptedSupplierJob(
                            quotedOpportunityData: _listOpportunityData[index],
                          )),
                );
                setState(() {
                  _setupQuotedList(); // New task expired, finished quote may be deleted
                });
              },
            );
          },
        ),
      ),
    );
  }
}
