import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../global.dart';
import '../libMyLib.dart';

import 'scProfile_1.dart';
import 'scChangePW.dart';
import 'scPrivacy.dart';

class MenuItem {
  String title;
  bool isCategory;

  MenuItem({
    this.title = '',
    this.isCategory = false,
  });
}

List<MenuItem> listItems = [
  MenuItem(title: 'Account', isCategory: true),
  MenuItem(title: 'Profile'),
  MenuItem(title: 'Change Password'),
  MenuItem(title: 'Other', isCategory: true),
  MenuItem(title: 'Privacy Policy'),
  MenuItem(title: 'Support'),
  MenuItem(title: 'Invite Friends'),
  MenuItem(title: 'Logout'),
];

class ScSettings extends StatelessWidget {
  //-------------------------
  //-- Switching Page Func --
  //-------------------------
  Future<void> _switchPage(context, index) async {
    String title = listItems[index].title;
    if (title == 'Profile') {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ScProfile1()),
      );
    } else if (title == 'Change Password') {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => ScChangePW()));
    } else if (title == 'Privacy Policy') {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => ScPrivacy()));
    } else if (title == 'Logout') {
      await libShowSelectionAlert(context, 'Logout ${globalUserData.username}?',
          textNo: 'Cancel', textYes: 'Logout', funcYes: () async {

        // clear login at backup data
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setBool('isLogin', false);
        // clear notification registration
        try {
          globalWebService.clrAndroidID(regID: null);
        } catch (e) {
          print(e);
        }
        Navigator.of(context)
            .pushNamedAndRemoveUntil('Login', (Route<dynamic> route) => false);
      });
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: listItems.length,
        itemBuilder: (context, index) {
          return listItems[index].isCategory
              //-- category
              ? Container(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  height: 40,
                  color: Colors.grey[200],
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      listItems[index].title,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              //-- to menu --
              : InkWell(
                  onTap: () async {
                    await _switchPage(context, index);
                  },
                  child: Container(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    height: 55,
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Colors.grey[200]),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          listItems[index].title,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        Icon(Icons.navigate_next),
                      ],
                    ),
                  ),
                );
        },
      ),
    );
  }
}
