import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';
import 'package:flutter/services.dart';

import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../global.dart';
import '../libWebData.dart';
import '../libGetPicture.dart';
import '../libMyLib.dart';
import '../libMapTools.dart';
import '../objects/objCacheImage.dart';
import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';

import 'scProfile_2.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScProfile1 extends StatefulWidget {
  @override
  _ScProfile1State createState() => _ScProfile1State();
}

class _ScProfile1State extends State<ScProfile1> {
  final FocusNode _focusEmail = FocusNode();
  final FocusNode _focusPhoneNumber = FocusNode();
  final FocusNode _focusAddress = FocusNode();

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();

  String _selectImageSource = '';
  File _image;

  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  Widget _bottomSheet(context) {
    return Container(
      child: Wrap(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.camera_alt),
              title: Text('Take a photo'),
              onTap: () {
                _selectImageSource = 'camera';
                Navigator.of(context).pop();
              }),
          ListTile(
            leading: Icon(Icons.image),
            title: Text('Image from gallery'),
            onTap: () {
              _selectImageSource = 'gallery';
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  //---------------------------------------------
  //---------------------------------------------
  // Get Image function
  //---------------------------------------------
  Future getImage() async {
    _selectImageSource = '';
    await showModalBottomSheet(context: context, builder: _bottomSheet);
    if (_selectImageSource.isEmpty) return;

    File savedImageFile = await getPicture(
      selectImageSource: _selectImageSource,
      isSquareImage: true,
    );
    if (savedImageFile == null) return;

    List<int> imageBytes = savedImageFile.readAsBytesSync();
    globalProfilePictureBase64 = base64Encode(imageBytes);

    setState(() {
      _image = savedImageFile;
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<void> _goNextOrUpdate(context) async {
    if (globalProfileData.username.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a username.');
      return;
    }
    if (globalProfileData.phone.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a phone number.');
      return;
    }
    if (globalProfileData.location.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a valid address.');
      return;
    }
    if (globalProfileData.location_lat == 0 ||
        globalProfileData.location_lon == 0) {
      try {
        LocationData result = await getLocation(globalProfileData.location);
        if (!result.isError) {
          globalProfileData.location_lat = result.lat;
          globalProfileData.location_lon = result.lng;
        } else {
          await libShowErrorAlert(context, 'Please enter a valid address.');
          return;
        }
      } catch (e) {
        print(e);
        await libShowErrorAlert(context, 'Please enter a valid address.');
        return;
      }
    }

    if (globalUserData.isSeller()) {
      // go next page...
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ScProfile2()),
      );
    } else {
      setState(() {
        _waiting = true;
      });
      try {
        await globalWebService.updateUser(funcSuccess: () async {
          Navigator.popUntil(context, ModalRoute.withName('Menu'));
        });
      } catch (e) {
        setState(() {
          _waiting = false;
        });
        String msg = e.toString();
        int idx = msg.indexOf(':') ?? -2;
        await libShowErrorAlert(context, msg.substring(idx + 2));
      }
    }
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();

    // init profile data instance with a clone of globalUserData
    globalProfileData = UserData.cloneFrom(globalUserData);

    // only Hire/Rent for future.
    globalProfileData.service_type = 'Hire/Rent';
    if (globalUserData.service_type != 'Hire/Rent')
      globalProfileData.category = [];

    globalProfilePictureBase64 = '';

    _nameController.text = globalProfileData.username;
    _emailController.text = globalProfileData.email;
    _phoneNumberController.text = globalProfileData.phone;
    _addressController.text = globalProfileData.location;
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() {
    super.didChangeDependencies();

    globalProfileData.printContent();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _phoneNumberController.dispose();
    _addressController.dispose();

    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Scaffold(
          //-------------------
          //-- Title App Bar --
          //-------------------
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              'Profile',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
          ),
          //--------------------------
          //-- body of entry fields --
          //--------------------------
          body: Container(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            //---------------------
                            //-- Picture content --
                            //---------------------
                            InkWell(
                              onTap: () => getImage(),
                              child: SizedBox(
                                width: 120,
                                height: 120,
                                child: Stack(
                                  children: <Widget>[
                                    //-- picture --
                                    (_image == null)
                                        ? CacheAvatar(
                                            imageURL: globalUserData.picture,
                                            radius: 60,
                                            iconSizeDummy: 60,
                                          )
                                        : Container(
                                            width: 120,
                                            height: 120,
                                            decoration: new BoxDecoration(
                                              image: DecorationImage(
                                                image: FileImage(_image),
                                                fit: BoxFit.fill,
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                          ),
                                    //-- edit icon --
                                    Align(
                                      alignment: Alignment(0, 1.4),
                                      child: Container(
                                        width: 40,
                                        height: 40,
                                        decoration: BoxDecoration(
                                          color: GLOBAL_THEME_COLOR,
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: Colors.white, width: 2),
                                        ),
                                        child: Icon(
                                          Icons.edit,
                                          color: Colors.white,
                                          size: 22.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            //--------------------
                            //-- name field --
                            //--------------------
                            EntryField(
                              label: 'Name',
                              onChanged: (text) {
                                globalProfileData.username = text;
                              },
                              onSubmitted: (value) {
                                FocusScope.of(context)
                                    .requestFocus(_focusEmail);
                              },
                              controller: _nameController,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                            ),
                            //--------------------
                            //-- email field --
                            //--------------------
                            EntryField(
                              onChanged: (text) {},
                              label: 'Email',
                              textColor: Colors.grey,
                              onSubmitted: (value) {
                                FocusScope.of(context)
                                    .requestFocus(_focusPhoneNumber);
                              },
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              focusNode: _focusEmail,
                              textInputAction: TextInputAction.next,
                              readOnly: true,
                            ),
                            //------------------------
                            //-- phone number field --
                            //------------------------
                            EntryField(
                              label: 'Phone Number',
                              onChanged: (text) {
                                globalProfileData.phone = text;
                              },
                              onSubmitted: (value) {
//                                FocusScope.of(context)
//                                    .requestFocus(_focusAddress);
                                FocusScope.of(context).unfocus();
                              },
                              controller: _phoneNumberController,
                              keyboardType: TextInputType.number,
                              focusNode: _focusPhoneNumber,
                              textInputAction: TextInputAction.next,
                            ),
                            //-----------------------
                            //-- Address TextField --
                            //-----------------------
                            EntryField(
                              readOnly: true,
                              onTap: () {
                                setState(() async {
                                  LocationResult result = await showLocationPicker(
                                    context,
                                    GLOBAL_MAP_KEY,
                                    initialCenter: LatLng(globalProfileData.location_lat, globalProfileData.location_lon),
                                    automaticallyAnimateToCurrentLocation: false,
                                    myLocationButtonEnabled: false,
                                    requiredGPS: false,
                                    layersButtonEnabled: false,
                                    appBarColor: Colors.black26,
                                  );
                                  _addressController.text = result.address;
                                  globalProfileData.location = result.address;
                                  globalProfileData.location_lat =
                                      result.latLng.latitude;
                                  globalProfileData.location_lon =
                                      result.latLng.longitude;
                                });
                              },
                              label: 'Address',
//                              onChanged: (text) {
//                                globalProfileData.location = text;
//                              },
//                              onSubmitted: (value) {
//                                FocusScope.of(context).unfocus();
//                              },
                              controller: _addressController,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.done,
                            ),
                            //----------------------
                            //-- Locate Me Button --
                            //-----------------------
//                            Align(
//                              alignment: Alignment.centerRight,
//                              child: FlatButton(
//                                onPressed: () {
//                                  setState(() async {
//                                    LocationResult result =
//                                        await showLocationPicker(
//                                      context,
//                                      GLOBAL_MAP_KEY,
//                                      initialCenter: LatLng(
//                                          GLOBAL_INIT_LAT, GLOBAL_INIT_LNG),
//                                      automaticallyAnimateToCurrentLocation:
//                                          false,
//                                      myLocationButtonEnabled: false,
//                                      requiredGPS: false,
//                                      layersButtonEnabled: false,
//                                      appBarColor: Colors.black26,
//                                    );
//                                    _addressController.text = result.address;
//                                    globalProfileData.location = result.address;
//                                    globalProfileData.location_lat =
//                                        result.latLng.latitude;
//                                    globalProfileData.location_lon =
//                                        result.latLng.longitude;
//                                  });
//                                },
//                                child: Text(
//                                  'Locate me',
//                                  style: TextStyle(
//                                    color: GLOBAL_THEME_COLOR,
//                                    fontSize: 16,
//                                    decoration: TextDecoration.underline,
//                                  ),
//                                ),
//                              ),
//                            ),
                          ]),
                    ),
                  ),
                  //---------------------------
                  //-- Next or Update button --
                  //---------------------------
                  AppButton(
                    title: globalProfileData.isSeller() ? 'Next' : 'Update',
                    onPressed: () async {
                      await _goNextOrUpdate(context);
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
