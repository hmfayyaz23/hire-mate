import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

import 'scSignUp_1.dart';
import '../global.dart';

class ScTerms extends StatefulWidget {
  @override
  _ScTermsState createState() => _ScTermsState();
}

class _ScTermsState extends State<ScTerms> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        centerTitle: true,
        title: Text(
          'TERMS AND CONDITIONS',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: WebView(
        initialUrl: 'https://conxhire.com.au/TermsAndConditions.html',
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 50,
          color: GLOBAL_THEME_COLOR,
          child: FlatButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ScSignUp1()),
              );
            },
            child: Text(
              'Agree',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
