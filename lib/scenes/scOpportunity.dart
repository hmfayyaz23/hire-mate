import 'package:flutter/material.dart';
import 'package:hire_mate_app/objects/objCacheImage.dart';
import 'package:hire_mate_app/objects/objTextDateFormat.dart';

import '../global.dart';
import '../objects/objAppButton.dart';
import '../libWebData.dart';

import 'scNewQuote.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScOpportunity extends StatelessWidget {
  final OpportunityData opportunityData;

  ScOpportunity({
    @required this.opportunityData,
  });

  //---------------------------------------------
  //---------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //-------------------
      //-- Title App Bar --
      //-------------------
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          'Opportunity',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    //------------------------
                    //-- Quotation headings --
                    //------------------------
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              opportunityData.title,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              (opportunityData.budget_price == null) ? 'Negotiate' : '\$${opportunityData.budget_price}',
                              style: TextStyle(
                                color: GLOBAL_THEME_COLOR,
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),
                        Row(
                          children: <Widget>[
                            TextDateFormat(
                              dateString: opportunityData.date_start,
                            ),
                            SizedBox(width: 20),
                            Icon(
                              Icons.location_on,
                              size: 18,
                              color: GLOBAL_THEME_COLOR,
                            ),
                            Container(
                              width: 160,
                              child: Text(
                                opportunityData.address,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: GLOBAL_THEME_COLOR,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    //------------------
                    //-- Photo images --
                    //------------------
                    CacheImage(
                      width: MediaQuery.of(context).size.width - 30,
                      height: (MediaQuery.of(context).size.width - 30)* 0.75,
                      imageURL: opportunityData.picture,
                    ),
                    SizedBox(height: 20),
                    //-----------------
                    //-- Description --
                    //-----------------
                    Text(
                      opportunityData.user_description,
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //-------------------------
            //-- Submit Quote button --
            //-------------------------
            AppButton(
              title: 'Submit quote',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ScNewQuote(
                    taskID: opportunityData.id,
                  )),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

//------------------------
