import 'package:flutter/material.dart';
import 'package:hire_mate_app/libWebData.dart';
import 'package:hire_mate_app/objects/objTextDateFormat.dart';

import '../global.dart';
import '../objects/objInfoCard.dart';
import '../objects/objTextDateFormat.dart';

import 'scOpportunity.dart';

class ScMeSupplier extends StatefulWidget {
  final double startPosition;
  ScMeSupplier({
    this.startPosition = 0,
  });

  @override
  _ScMeSupplierState createState() => _ScMeSupplierState();
}

class _ScMeSupplierState extends State<ScMeSupplier> {
  // RefreshIndicator requires key to implement state (for RefreshIndicator)
  final GlobalKey<RefreshIndicatorState> _refreshKey =
      GlobalKey<RefreshIndicatorState>();
  // For scrolling control
  ScrollController _scrollController;
  double _offset;

  List<OpportunityData> _listOpportunityData = [];

  bool _isUpdating = false;

  //---------------------------------------------
  // remove finished or quoted opportunities
  //---------------------------------------------
  void _setupOpportunityList() {
    if (!globalUserData.isSeller()) return;

    _listOpportunityData = [];
    globalSupplierEnquiresList.forEach((element) {
      bool isListed = true;

      //element.printContent();

      if (element.task_finished >= 1) {
        // task was finished
        isListed = false;
      } else {
        int taskId = element.id;
        globalSupplierQuotedList.forEach((e) {
          // task was quoted
          if (e.task.id == taskId) isListed = false;
        });
      }
      if (isListed) _listOpportunityData.add(element);
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<Null> _refresh() async {
    _isUpdating = true;

    try {
      await globalWebService.getSellerTask();
    } catch (e) {
      print(e);
    }
    _isUpdating = false;
    _setupOpportunityList();

    setState(() {});

    return null;
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScMeSupplier]initState...');

    // Init ListView scroller control
    _offset = widget.startPosition;
    _scrollController =
        ScrollController(initialScrollOffset: widget.startPosition);
    _scrollController.addListener(() {
      _offset = _scrollController.offset;
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScMeSupplier]didChangeDependencies...');

    _setupOpportunityList();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    globalOffsetScMeSupplier = _offset;

    print("[ScMeSupplier]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return !globalUserData.isSeller()
        ? Container(
            padding: EdgeInsets.all(15),
            child: Center(
                child: Text(
              'You are not subscripted as a Supplier.',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.black54,
              ),
            )),
          )
        : Container(
            margin: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //-------------
                //-- My jobs --
                //-------------
                Padding(
                  padding: const EdgeInsets.only(left: 8, bottom: 5),
                  child: Text(
                    'My opportunies',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
                //---------------
                //-- List View --
                //---------------
                Expanded(
                  child: RefreshIndicator(
                    key: _refreshKey,
                    onRefresh: _refresh,
                    child: ListView.builder(
                      physics: AlwaysScrollableScrollPhysics(),
                      controller: _scrollController,
                      itemCount: _listOpportunityData.length,
                      itemBuilder: (context, index) {
                        return InfoCard(
                          imgLink: _listOpportunityData[index].picture,
                          title: _listOpportunityData[index].title,
                          price:
                              '\$${_listOpportunityData[index].budget_price}',
                          location: _listOpportunityData[index].address,
                          startDate: getFormattedString(_listOpportunityData[index].date_start),
                          subtitle: _listOpportunityData[index].user_description,
                          onTap: () async {
                            await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ScOpportunity(
                                        opportunityData:
                                            _listOpportunityData[index],
                                      )),
                            );
                            setState(() {
                              _setupOpportunityList(); //new quote may be submitted
                            });
                          },
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}
