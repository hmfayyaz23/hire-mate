import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../global.dart';
import '../libMyLib.dart';
import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';

import 'scSignUp_1.dart';
import 'scTerms.dart';
import 'scForgotPassword.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScLogin extends StatefulWidget {
  @override
  _ScLoginState createState() => _ScLoginState();
}

class _ScLoginState extends State<ScLogin> {
  String _inputEmail = '';
  String _inputPassword = '';
//  String _inputEmail = 'wilsonwansun@gmail.com';
//  String _inputPassword = '123456';
  final FocusNode _focusPassword = FocusNode();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  //---------------------------------------------
  //-- Go Login Function --
  //---------------------------------------------
  Future<void> _goLogin() async {
    if (!libCheckValidEmail(_inputEmail)) {
      await libShowErrorAlert(context, 'Please enter valid email address');
      return;
    }
    if (_inputPassword.isEmpty) { 
      await libShowErrorAlert(context, 'Please enter password');
      return;
    }

    setState(() {
      _waiting = true;
    });

    try {
      await globalWebService.login(
          username: _inputEmail,
          password: _inputPassword,
          funcSuccess: () async {
            //setState(() {
            //  _waiting = true;
            //});

            globalUserData.printContent();
            //getUser or getSeller is run within the login function...
            //getUserTask and getSellerTask are run within the login function...

            //backup login parameters
            SharedPreferences prefs = await SharedPreferences.getInstance();
            await prefs.setBool('isLogin', true);
            await prefs.setString('userEmail', _inputEmail);
            await prefs.setString('userPassword', _inputPassword);

            Navigator.popAndPushNamed(context, 'Menu');
          });
    } catch (e) {
      setState(() {
        _waiting = false;
      });
      String msg = e.toString();
      int idx = msg.indexOf(':') ?? -2;
      await libShowErrorAlert(context, msg.substring(idx + 2));
    }
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() {
    super.didChangeDependencies();

    _emailController.text = _inputEmail;
    _passwordController.text = _inputPassword;
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: ProgressHUD(
        inAsyncCall: _waiting,
        child: Scaffold(
          body: Container(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 40),
                          //----------
                          //-- Logo --
                          //----------
                          Hero(
                            tag: 'logo',
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(30.0),
                              child: Image.asset(
                                'images/logo.png',
                                width: 150.0,
                                height: 150.0,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          SizedBox(height: 15),
                          //---------------
                          //-- Welcome! ---
                          //---------------
                          Text(
                            'Welcome!',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 22.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 10),
                          //-------------------------------
                          //-- Please login your account --
                          //-------------------------------
                          Text(
                            'Please login your account',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                            ),
                          ),
                          SizedBox(height: 10),
                          //--------------------
                          //-- email field --
                          //--------------------
                          EntryField(
                            label: 'Email',
                            onChanged: (text) {
                              _inputEmail = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context)
                                  .requestFocus(_focusPassword);
                            },
                            controller: _emailController,
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.next,
                          ),
                          //--------------------
                          //-- password field --
                          //--------------------
                          EntryField(
                            label: 'Password',
                            isObscureText: true,
                            onChanged: (text) {
                              _inputPassword = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            controller: _passwordController,
                            focusNode: _focusPassword,
                            textInputAction: TextInputAction.done,
                          ),
                          SizedBox(height: 18),
                          //----------------------------
                          //-- forgot password button --
                          //----------------------------
                          Align(
                            alignment: Alignment.centerRight,
                            child: InkWell(
                              child: Text(
                                'Forgot Password?',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.grey,
                                ),
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ScForgotPassword()),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  //------------------
                  //-- login button --
                  //------------------
                  AppButton(
                    title: 'Login',
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      await _goLogin();
                    }
                  ),
                  SizedBox(height: 10),
                  //--------------------
                  //-- sign up button --
                  //--------------------
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Don\'t have an account?',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 16.0,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                //builder: (context) => ScSignUp1()),
                                builder: (context) => ScTerms()),
                          );
                        },
                        child: Text(
                          ' SIGN UP',
                          style: TextStyle(
                            color: GLOBAL_THEME_COLOR,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                Text('Ver. $globalVersionNumber',
                  style: TextStyle(
                    color: GLOBAL_THEME_COLOR.withAlpha(128),
                    fontSize: 10,
                  ),
                ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
