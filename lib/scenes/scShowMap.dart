import 'package:flutter/material.dart';

import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../global.dart';

class ScShowMap extends StatefulWidget {
  final String address;
  final LatLng location;
  final double distance;

  ScShowMap({
    this.address = '',
    @required this.location,
    this.distance = 0,
  });

  @override
  _ScShowMapState createState() => _ScShowMapState();
}

class _ScShowMapState extends State<ScShowMap> {
  Completer<GoogleMapController> _controller = Completer();

  Set<Marker> _markers = Set();

  Set<Circle> _getRange() {
    return Set.from([
      Circle(
        circleId: CircleId('8888'),
        center: widget.location,
        radius: widget.distance * 1000,
        fillColor: Color(0x1fff0000),
        strokeColor: Colors.transparent,
        strokeWidth: 2,
      )
    ]);
  }

  @override
  Widget build(BuildContext context) {
    print(widget.location);

    double _getZoomValue() {
      double d = widget.distance;
      if (d <= 5) return 12;
      if (d <= 10) return 11;
      if (d <= 20) return 10;
      if (d <= 50) return 9;
      if (d <= 100) return 8;
      if (d <= 1000) return 4;
      return 2;
    }

    _markers.add(
      Marker(
        markerId: MarkerId('marker_id'),
        position: widget.location,
        infoWindow: InfoWindow(
          title: 'Hello',
          snippet: 'World',
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          widget.address,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: CameraPosition(
            target: widget.location,
            zoom: _getZoomValue(),
          ),
          markers: _markers,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          circles: _getRange(),
        ),
      ),
    );
  }
}
