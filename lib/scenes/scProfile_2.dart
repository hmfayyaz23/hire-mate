import 'package:flutter/material.dart';

import '../global.dart';
import '../libWebData.dart';
import '../libMyLib.dart';
import '../libMapTools.dart';
import '../objects/objAppButton.dart';
import '../objects/objEntryField.dart';


import 'scManageLocations.dart';
import 'scSelectServices.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScProfile2 extends StatefulWidget {
  @override
  _ScProfile2State createState() => _ScProfile2State();
}

class _ScProfile2State extends State<ScProfile2> {
  bool _waiting = false;

  final TextEditingController _abnController = TextEditingController();
  final TextEditingController _businessTypeController = TextEditingController();
  final TextEditingController _businessTradingNameController = TextEditingController();

  final FocusNode _focusBusinessType = FocusNode();
  final FocusNode _focusBusinessTradingName = FocusNode();

  Future<void> _goUpdate(context) async {
    if (globalProfileData.abn.isEmpty) {
      await libShowErrorAlert(context, 'Please enter ABN.');
      return;
    }
    if (globalProfileData.business_trading_name.isEmpty) {
      await libShowErrorAlert(context, 'Please enter Business Trading Name.');
      return;
    }
    if (globalProfileData.service_locations.isEmpty) {
      await libShowErrorAlert(context, 'Please enter at least one service location.');
      return;
    }
    if (globalProfileData.category.isEmpty) {
      await libShowErrorAlert(context, 'Please select at least one service category.');
      return;
    }

    setState(() {
      _waiting = true;
    });
    try {
      await globalWebService.updateUser(funcSuccess: () async {
        Navigator.popUntil(context, ModalRoute.withName('Menu'));
      });
    } catch (e) {
      setState(() {
        _waiting = false;
      });
      String msg = e.toString();
      int idx = msg.indexOf(':') ?? -2;
      await libShowErrorAlert(context, msg.substring(idx + 2));
    }
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();

    _abnController.text = globalProfileData.abn;
    _businessTypeController.text = globalProfileData.business_type;
    _businessTradingNameController.text = globalProfileData.business_trading_name;
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    _abnController.dispose();
    _businessTypeController.dispose();
    _businessTradingNameController.dispose();

    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Scaffold(
          //-------------------
          //-- Title App Bar --
          //-------------------
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              'Profile',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
          ),
          //--------------------------
          //-- body of entry fields --
          //--------------------------
          body: Container(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            //----------------------
                            //-- ABN --
                            //----------------------
                            EntryField(
                              label: 'ABN',
                              isDense: true,
                              onChanged: (text) {
                                globalProfileData.abn = text;
                              },
                              onSubmitted: (value) {
                                FocusScope.of(context).requestFocus(_focusBusinessType);
                              },
                              controller: _abnController,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                            ),
                            //-------------------
                            //-- Business Type --
                            //-------------------
                            EntryField(
                              label: 'Business Type (Optional)',
                              isDense: true,
                              onChanged: (text) {
                                globalProfileData.business_type = text;
                              },
                              onSubmitted: (value) {
                                FocusScope.of(context).requestFocus(_focusBusinessTradingName);
                              },
                              controller: _businessTypeController,
                              focusNode: _focusBusinessType,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                            ),
                            //---------------------------
                            //-- Business Trading Name --
                            //---------------------------
                            EntryField(
                              label: 'Business Trading Name',
                              isDense: true,
                              onChanged: (text) {
                                globalProfileData.business_trading_name = text;
                              },
                              onSubmitted: (value) {
                                FocusScope.of(context).unfocus();
                              },
                              controller: _businessTradingNameController,
                              focusNode: _focusBusinessTradingName,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.done,
                            ),
                            SizedBox(height: 30),
                            //------------------------------
                            //-- Manage service locations --
                            //------------------------------
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Service Locations',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 20,
                                  //fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            NextPageButton(
                              title: 'Manage service location',
                              icon: Icons.location_on,
                              onTap: () async {
                                var list = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ScManageLocations(
                                            listServiceLocation:
                                                globalProfileData.service_locations,
                                          )),
                                );

                                // replace new saved data
                                if (list is List<ServiceLocation>)
                                  globalProfileData.service_locations = list;
                              },
                            ),
                            SizedBox(height: 10),
                            //------------------
                            //------------------
                            //-- Service type --
                            //------------------
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Service Type',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 20,
                                  //fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(height: 15),
                            //-----------------
                            //-- Hire/Rental --
                            //-----------------
                            NextPageButton(
                              title: 'Hire/Rental',
                              icon: Icons.title,
                              color: Colors.black,
                              count: '${globalProfileData.category.length}/${GLOBAL_SERVICES['Hire/Rental'].length - 4}',
                              onTap: () async {
                                var result = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ScSelectServices(
                                            choice: 'Hire/Rental',
                                            selectedList: globalProfileData.category,
                                          )),
                                );
                                if (result is List<String>) {
                                  setState(() {
                                    globalProfileData.category = result;
                                  });
                                }
                              },
                            ),
                          ]),
                    ),
                  ),
                  AppButton(
                    title: 'Update',
                    onPressed: () async {
                      await _goUpdate(context);
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
