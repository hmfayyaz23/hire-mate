import 'package:flutter/material.dart';

import '../global.dart';
import '../libMyLib.dart';
import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScChangePW extends StatefulWidget {
  @override
  _ScChangePWState createState() => _ScChangePWState();
}

class _ScChangePWState extends State<ScChangePW> {
  String _inputPassword = '';
  String _inputPassword1 = '';
  String _inputPassword2 = '';

  final FocusNode _focusPassword1 = FocusNode();
  final FocusNode _focusPassword2 = FocusNode();

  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _password1Controller = TextEditingController();
  final TextEditingController _password2Controller = TextEditingController();

  bool _waiting = false;

  Future<void> _goUpdate() async {
    if (_inputPassword.isEmpty) {
      await libShowErrorAlert(context, 'Please enter current password.');
      return;
    }
    if (_inputPassword != globalWebService.userPassword) {
      await libShowErrorAlert(context, 'Current password incorrect.');
      return;
    }
    if (_inputPassword1.isEmpty || _inputPassword2.isEmpty) {
      await libShowErrorAlert(context, 'Please enter new password.');
      return;
    }
    if (_inputPassword1 != _inputPassword2) {
      await libShowErrorAlert(context, 'The new passwords are not matched.');
      return;
    }

    setState(() {
      _waiting = true;
    });
    try {
      await globalWebService.updateUser(
          password: _inputPassword1, //--> update password only
          funcSuccess: () async {
            setState(() {
              _waiting = false;
            });
            await libShowErrorAlert(context, 'Password updated successfully!');
            Navigator.of(context).pop();
          });
    } catch (e) {
      setState(() {
        _waiting = false;
      });
      String msg = e.toString();
      int idx = msg.indexOf(':') ?? -2;
      await libShowErrorAlert(context, msg.substring(idx + 2));
    }
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Scaffold(
          //-------------------
          //-- Title App Bar --
          //-------------------
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              'New account',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
          ),
          //--------------------------
          //-- body of entry fields --
          //--------------------------
          body: Container(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          //--------------------
                          //-- password field --
                          //--------------------
                          EntryField(
                            label: 'Current Password',
                            isObscureText: true,
                            onChanged: (text) {
                              _inputPassword = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).requestFocus(_focusPassword1);
                            },
                            controller: _passwordController,
                            textInputAction: TextInputAction.next,
                          ),
                          //--------------------
                          //-- password field --
                          //--------------------
                          EntryField(
                            label: 'New Password',
                            isObscureText: true,
                            onChanged: (text) {
                              _inputPassword1 = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).requestFocus(_focusPassword2);
                            },
                            controller: _password1Controller,
                            focusNode: _focusPassword1,
                            textInputAction: TextInputAction.next,
                          ),
                          //--------------------
                          //-- password field --
                          //--------------------
                          EntryField(
                            label: 'Re-Enter Password',
                            isObscureText: true,
                            onChanged: (text) {
                              _inputPassword2 = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            controller: _password2Controller,
                            focusNode: _focusPassword2,
                            textInputAction: TextInputAction.done,
                          ),
                        ],
                      ),
                    ),
                  ),
                  //------------------
                  //-- Next button --
                  //------------------
                  AppButton(
                    title: 'Update',
                    onPressed: () async {
                      await _goUpdate();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void dispose() {
    _passwordController.dispose();
    _password1Controller.dispose();
    _password2Controller.dispose();
    super.dispose();
  }
}
