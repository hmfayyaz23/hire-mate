import 'package:flutter/material.dart';
import 'dart:async'; //for timer

import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../global.dart';
import 'scLogin.dart';

class ScSplash extends StatefulWidget {
  @override
  _ScSplashState createState() => _ScSplashState();
}

class _ScSplashState extends State<ScSplash> {
  @override
  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      //String appName = packageInfo.appName;
      //String packageName = packageInfo.packageName;
      globalVersionNumber = packageInfo.version;
      globalBuildNumber = packageInfo.buildNumber;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLogin = prefs.getBool('isLogin') ?? false;
    //if (false) {
    if (isLogin) {
      String userEmail = prefs.getString('userEmail');
      String userPassword = prefs.getString('userPassword');

      try {
        await globalWebService.login(
            username: userEmail,
            password: userPassword,
            funcSuccess: () async {
              globalUserData.printContent();
              //getUser or getSeller is run within the login function...
              //getUserTask and getSellerTask are run within the login function...
              Navigator.popAndPushNamed(context, 'Menu');
            });
      } catch (e) {
        print(e);
        Navigator.pushReplacement(
          context,
          //fading transition
          PageRouteBuilder(
            pageBuilder: (c, a1, a2) => ScLogin(),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 1200),
          ),
        );
      }
    } else {
      Timer(Duration(seconds: 3), () async {
        Navigator.pushReplacement(
          context,
          //fading transition
          PageRouteBuilder(
            pageBuilder: (c, a1, a2) => ScLogin(),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 1200),
          ),
        );
      });
    }
  }

// sliding transition
//          PageRouteBuilder(
//            transitionDuration: Duration(milliseconds: 1200),
//            pageBuilder: (
//              BuildContext context,
//              Animation<double> animation,
//              Animation<double> secondaryAnimation,
//            ) =>
//                ScLogin(),
//            transitionsBuilder: (
//              BuildContext context,
//              Animation<double> animation,
//              Animation<double> secondaryAnimation,
//              Widget child,
//            ) =>
//                SlideTransition(
//              position: Tween<Offset>(
//                begin: const Offset(0, 1),
//                end: Offset.zero,
//              ).animate(animation),
//              child: child,
//            ),
//          )

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Container(
//        color: Color(0xffd3192e),
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.orange,
                GLOBAL_THEME_COLOR,
                GLOBAL_THEME_COLOR,
              ]),
        ),
        //color: Color(0xffe7132a),
        child: Center(
          child: Hero(
            tag: 'logo',
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image.asset(
                'images/logo.png',
                width: 100.0,
                height: 100.0,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
