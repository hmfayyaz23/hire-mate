import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';

import '../global.dart';
import '../libWebData.dart';
import '../libMyLib.dart';

import '../objects/objInfoCard.dart';
import '../objects/objAppButton.dart';
import '../objects/objCacheImage.dart';
import '../objects/objTextDateFormat.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScAcceptJobs extends StatefulWidget {
  final QuoteData acceptedQuote;

  ScAcceptJobs({
    @required this.acceptedQuote,
  });

  @override
  _ScAcceptJobsState createState() => _ScAcceptJobsState();
}

class _ScAcceptJobsState extends State<ScAcceptJobs> {
  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  Future<void> _goFinish() async {
    await libShowSelectionAlert(
        context, 'Finish job of \"${widget.acceptedQuote.task.title}\"?',
        textNo: 'Cancel', textYes: 'Finish', funcYes: () async {
      setState(() {
        _waiting = true;
      });
      try {
        await globalWebService.finishTask(
            id: widget.acceptedQuote.task.id,
            funcSuccess: () async {
              // update user task list for the removed task.
              await globalWebService.getUserTask();
              Navigator.of(context).pop();
            });
      } catch (e) {
        setState(() {
          _waiting = false;
        });
        String msg = e.toString();
        int idx = msg.indexOf(':') ?? -2;
        await libShowErrorAlert(context, msg.substring(idx + 2));
      }
    });
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Scaffold(
          //-------------------
          //-- Title App Bar --
          //-------------------
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              'Accept Jobs',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
          ),
          //--------------------------
          //-- body of entry fields --
          //--------------------------
          body: Container(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  //-- Title --
                                  Text(
                                    widget.acceptedQuote.task.title,
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  //-- Date --
                                  TextDateFormat(
                                    dateString: widget.acceptedQuote.task.date_start,
                                  ),
                                ],
                              ),
                              //-- quoted price --
                              Text(
                                '\$${widget.acceptedQuote.quote_price}',
                                style: TextStyle(
                                  color: GLOBAL_THEME_COLOR,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          //-- seller image --
                          CacheAvatar(
                            imageURL: widget.acceptedQuote.seller.picture,
                            radius: 60,
                            iconSizeLoading: 40,
                            iconSizeDummy: 60,
                          ),
                          SizedBox(height: 10),
                          //-- Name --
                          ContentBlock(
                            title: 'Name',
                            content: widget.acceptedQuote.seller.username,
                          ),
                          //-- Email --
                          ContentBlock(
                            title: 'Email',
                            content: widget.acceptedQuote.seller.email,
                            icon: Icons.email,
                            onTap: () async {
                              String url = 'mailto:' + widget.acceptedQuote.seller.email;

                              if (await canLaunch(url)) {
                              await launch(url);
                              } else {
                              await libShowErrorAlert(context, 'Error in sending the mail.');
                              }
                            },
                          ),
                          //-- Phone number --
                          ContentBlock(
                            title: 'Phone Number',
                            content: widget.acceptedQuote.seller.phone,
                            icon: Icons.phone,
                            onTap: () async {
                              String url = 'tel:' + widget.acceptedQuote.seller.phone;

                              if (await canLaunch(url)) {
                              await launch(url);
                              } else {
                              await libShowErrorAlert(context, 'Error in calling the phone.');
                              }
                            },
                          ),
                          SizedBox(height: 20),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              widget.acceptedQuote.seller_description,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ]),
                  ),
                ),
                AppButton(
                  title: 'Finish Job',
                  onPressed: () async {
                    await _goFinish();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ContentBlock extends StatelessWidget {
  final String title;
  final String content;
  final IconData icon;
  final Function onTap;

  const ContentBlock({
    @required this.title,
    @required this.content,
    this.onTap,
    this.icon,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  content,
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ],
            ),
            icon != null
                ? CircleButton(
                    onTap: onTap,
                    iconData: icon,
                  )
                : Container(),
          ],
        ),
        SizedBox(height: 5),
        Divider(color: Colors.grey),
      ],
    );
  }
}
