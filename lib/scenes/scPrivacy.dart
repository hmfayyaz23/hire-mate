import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

import '../global.dart';

class ScPrivacy extends StatefulWidget {
  @override
  _ScPrivacyState createState() => _ScPrivacyState();
}

class _ScPrivacyState extends State<ScPrivacy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        centerTitle: true,
        title: Text(
          'PRIVACY POLICY',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: WebView(
        initialUrl: 'https://hire-mate.com.au/privacy/',
      ),
    );
  }
}
