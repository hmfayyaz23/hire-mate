import 'package:flutter/material.dart';

import '../global.dart';
import '../libWebData.dart';
import '../objects/objInfoCard.dart';
import '../objects/objTextDateFormat.dart';

import 'scAcceptedSupplierJob.dart';

class ScAcceptedSupplierQuoted extends StatefulWidget {
  @override
  _ScAcceptedSupplierQuotedState createState() => _ScAcceptedSupplierQuotedState();
}

class _ScAcceptedSupplierQuotedState extends State<ScAcceptedSupplierQuoted> {
  // RefreshIndicator requires key to implement state (for RefreshIndicator)
  final GlobalKey<RefreshIndicatorState> _refreshKey = GlobalKey<RefreshIndicatorState>();
  // For scrolling control
  ScrollController _scrollController;

  List<QuotedOpportunityData> _listOpportunityData = [];

  bool _isUpdating = false;

  //---------------------------------------------
  //---------------------------------------------
  void _setupQuotedList() {
//    _listOpportunityData = globalSupplierQuotedList;
    _listOpportunityData = [];
    globalSupplierQuotedList.forEach((element) {
      if (element.isAccepted == 0) _listOpportunityData.add(element);
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  String _getSubtitle(index) {
    String result;
    if (_listOpportunityData[index].isDeclined != 0)
      result = 'Declined by customer.';
    else
      //result = getFormattedString(_listOpportunityData[index].task.date_start);
      result = 'Waiting customer reply.';

    return result;
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<Null> _refresh() async {
    _isUpdating = true;
    await globalWebService.getSellerTask(funcSuccess: () {
      _setupQuotedList();
      _isUpdating = false;
    });

    setState(() {});

    return null;
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScAcceptedSupplierQuoted]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScAcceptedSupplierQuoted]didChangeDependencies...');

    _setupQuotedList();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScAcceptedSupplierQuoted]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Container(
      child: RefreshIndicator(
        key: _refreshKey,
        onRefresh: _refresh,
        child: ListView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          controller: _scrollController,
          itemCount: _listOpportunityData.length,
          itemBuilder: (context, index) {
            return InfoCard(
              imgLink: _listOpportunityData[index].picture,
              title: _listOpportunityData[index].task.title,
              location: _listOpportunityData[index].task.address,
              startDate: getFormattedString(_listOpportunityData[index].task.date_start),
              subtitle: _getSubtitle(index),
              onTap: () async {
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ScAcceptedSupplierJob(
                            quotedOpportunityData: _listOpportunityData[index],
                          )),
                );
                setState(() {
                  _setupQuotedList(); //quote may be deleted
                });
              },
            );
          },
        ),
      ),
    );
  }
}
