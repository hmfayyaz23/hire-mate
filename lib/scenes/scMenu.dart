import 'dart:io';

import 'package:flutter/material.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:provider/provider.dart';

import '../global.dart';
import '../libMyLib.dart';
import '../objects/objCacheImage.dart';

import 'scMeCustomer.dart';
import 'scMeSupplier.dart';
import 'scAcceptedCustomer.dart';
import 'scAcceptedSupplier.dart';
import 'scNotificationsCustomer.dart';
import 'scNotificationsSupplier.dart';
import 'scSettings.dart';
import 'scProfile_1.dart';

//---------------------------------------------
class ScMenu extends StatefulWidget {
  ScMenu({Key key}) : super(key: key);

  @override
  _ScMenuState createState() => _ScMenuState();
}

//---------------------------------------------
class _ScMenuState extends State<ScMenu> {
  // notifications
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  // page view
  int _selectedIndex = 0;
  PageController _pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  // provider model
  NotificationModel notificationModel;

  bool _isCustomer = true;

  BuildContext buildContext;

  //---------------------------------------------
  //---------------------------------------------
  void processMessage(Map<String, dynamic> message, bool isAlert) {
    String dataType =
        Platform.isIOS ? message['data_type'] : message['data']['data_type'];

    void alertAndNotify() {
      if (isAlert) {
        libShowErrorAlert(
            context,
            Platform.isIOS
                ? message['aps']['alert']['body']
                : message['notification']['body']);
      }
      notificationModel.setNewNotification();
    }

    try {
      globalWebService.getNotification(funcSuccess: () {
        //globalNotificationList.forEach((element) {
        //  print('-----');
        //  element.printContent();
        //});
        if (dataType == 'quoted') {
          globalWebService.getUserTask(funcSuccess: () {
            alertAndNotify();
          });
        } else {
          globalWebService.getSellerTask(funcSuccess: () {
            alertAndNotify();
          });
        }
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[scMenu]initState...');

    //reset menu index
    globalAcceptedCustomerIndex = 0;
    globalAcceptedSupplierIndex = 0;

    notificationModel = NotificationModel();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        processMessage(message, true);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        processMessage(message, false);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        processMessage(message, false);
      },
    );

    _firebaseMessaging
        .requestNotificationPermissions(const IosNotificationSettings(
      sound: true,
      badge: true,
      alert: true,
      provisional: true,
    ));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    _firebaseMessaging.getToken().then((String token) {
      //assert(token != null);
      //print('Token: $token');
      globalWebService.setAndroidID(
          regID: token,
          funcSuccess: () {
            print('Android regID set.');
          });
    });

    //_firebaseMessaging.subscribeToTopic('wilson_testing');
    //_firebaseMessaging.unsubscribeFromTopic('wilson_testing');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[scMenu]didChangeDependencies...');

    notificationModel.setNewNotification();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[scMenu]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    buildContext = context;
    return ChangeNotifierProvider<NotificationModel>(
      create: (context) => notificationModel,
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: Column(
            children: <Widget>[
              SizedBox(height: 30),
              //------------
              //-- Header --
              //------------
              Container(
                width: double.infinity,
                height: 60,
                //color: Colors.grey,
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 10),
                    //---------------------
                    //---------------------
                    //-- Customer button --
                    //---------------------
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          if (!_isCustomer) {
                            setState(() {
                              _isCustomer = true;
                            });
                          }
                        },
                        //-- container --
                        child: Container(
                          height: 40,
                          width: 100,
                          decoration: BoxDecoration(
                            color:
                                _isCustomer ? GLOBAL_THEME_COLOR : Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                bottomLeft: Radius.circular(20)),
                            border: Border.all(
                                width: 1,
                                color: GLOBAL_THEME_COLOR,
                                style: BorderStyle.solid),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              //-- title --
                              Text(
                                "Customer",
                                style: TextStyle(
                                    color: _isCustomer
                                        ? Colors.white
                                        : GLOBAL_THEME_COLOR,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(width: 5),
                              //-- badge number --
                              Consumer<NotificationModel>(
                                builder: (co, mo, ch) => Visibility(
                                  visible: (mo.countCustomer != 0),
                                  child: Container(
                                    width: 22,
                                    height: 22,
                                    decoration: BoxDecoration(
                                      color: _isCustomer
                                          ? Colors.white
                                          : Colors.red,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Center(
                                      child: Text(
                                        mo.countCustomer.toString(),
                                        style: TextStyle(
                                          color: _isCustomer
                                              ? Colors.red
                                              : Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    //---------------------
                    //---------------------
                    //-- Supplier button --
                    //---------------------
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          if (_isCustomer) {
                            setState(() {
                              _isCustomer = false;
                            });
                          }
                        },
                        //-- container --
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                            color:
                                _isCustomer ? Colors.white : GLOBAL_THEME_COLOR,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20)),
                            border: Border.all(
                                width: 1,
                                color: GLOBAL_THEME_COLOR,
                                style: BorderStyle.solid),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              //-- title --
                              Text(
                                "Supplier",
                                style: TextStyle(
                                    color: !_isCustomer
                                        ? Colors.white
                                        : GLOBAL_THEME_COLOR,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(width: 5),
                              //-- badge number --
                              Consumer<NotificationModel>(
                                builder: (co, mo, ch) => Visibility(
                                  visible: (mo.countSupplier != 0),
                                  child: Container(
                                    width: 22,
                                    height: 22,
                                    decoration: BoxDecoration(
                                      color: !_isCustomer
                                          ? Colors.white
                                          : Colors.red,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Center(
                                      child: Text(
                                        mo.countSupplier.toString(),
                                        style: TextStyle(
                                          color: !_isCustomer
                                              ? Colors.red
                                              : Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    //-----------------
                    //-----------------
                    //-- User avatar --
                    //-----------------
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ScProfile1()),
                        );
                      },
                      child: CacheAvatar(
                        imageURL: (globalUserData == null)
                            ? false
                            : globalUserData.picture,
                      ),
                    ),
                    SizedBox(width: 10),
                  ],
                ),
              ),
              //----------------
              //----------------
              //-- Page view ---
              //----------------
              Expanded(
                child: PageView(
                  controller: _pageController,
                  //physics: NeverScrollableScrollPhysics(), //disable swipe
                  onPageChanged: (index) {
                    print('page changed');
                    setState(() {
                      _selectedIndex = index;
                    });
                  },
                  children: <Widget>[
                    (_isCustomer
                        ? ScMeCustomer(
                            startPosition: globalOffsetScMeCustomer ?? 0)
                        : ScMeSupplier(
                            startPosition: globalOffsetScMeSupplier ?? 0)),
                    (_isCustomer ? ScAcceptedCustomer() : ScAcceptedSupplier()),
                    (_isCustomer
                        ? Consumer<NotificationModel>(
                            builder: (co, mo, ch) =>
                                ScNotificationsCustomer(mo: mo))
                        : Consumer<NotificationModel>(
                            builder: (co, mo, ch) =>
                                ScNotificationsSupplier(mo: mo))),
                    ScSettings(),
                  ],
                ),
              ),
            ],
          ),
          //--------------------------
          //--------------------------
          //-- bottom navigator bar --
          //--------------------------
          bottomNavigationBar: BottomNavigationBar(
            iconSize: 40,
            unselectedFontSize: 0,
            selectedFontSize: 14,
            unselectedItemColor: Colors.black26,
            selectedIconTheme: IconThemeData(color: GLOBAL_THEME_COLOR),
            items: <BottomNavigationBarItem>[
              //-- Me --
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                title: Text('Me', style: TextStyle(color: GLOBAL_THEME_COLOR)),
              ),
              //-- Active --
              BottomNavigationBarItem(
                icon: Icon(Icons.assignment),
                title:
                    Text('Active', style: TextStyle(color: GLOBAL_THEME_COLOR)),
              ),
              //-- Notifications --
              BottomNavigationBarItem(
                icon: Consumer<NotificationModel>(
                  builder: (co, mo, ch) => IconWithBadge(count: mo.countTotal),
                ),
                activeIcon: Consumer<NotificationModel>(
                  builder: (co, mo, ch) => IconWithBadge(count: mo.countTotal),
                ),
                title: Text('Notifications',
                    style: TextStyle(color: GLOBAL_THEME_COLOR)),
              ),
              //-- Settings --
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title: Text('Settings',
                    style: TextStyle(color: GLOBAL_THEME_COLOR)),
              ),
            ],
            currentIndex: _selectedIndex,
            selectedItemColor: GLOBAL_THEME_COLOR,
            type: BottomNavigationBarType.shifting,
            onTap: (int index) {
              setState(() {
                _selectedIndex = index;
                //pageController.jumpToPage(index);
                _pageController.animateToPage(index,
                    duration: Duration(milliseconds: 400), curve: Curves.ease);
              });
            },
          ),
        ),
      ),
    );
  }
}

//---------------------------------------------
//---------------------------------------------
class IconWithBadge extends StatelessWidget {
  final int count;
  const IconWithBadge({
    Key key,
    this.count = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Center(child: Icon(Icons.notifications)),
        Visibility(
          visible: (count != 0),
          child: Align(
            alignment: Alignment(0.3, 0),
            child: Container(
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                color: Colors.red,
                shape: BoxShape.circle,
              ),
              child: Center(
                child: Text(
                  count.toString(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
