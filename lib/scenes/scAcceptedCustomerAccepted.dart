import 'package:flutter/material.dart';
import 'package:hire_mate_app/libMyLib.dart';

import 'package:url_launcher/url_launcher.dart';

import '../global.dart';
import '../objects/objInfoCard.dart';
import '../libWebData.dart';
import 'scAcceptJobs.dart';

class ScAcceptedCustomerAccepted extends StatefulWidget {
  @override
  _ScAcceptedCustomerAcceptedState createState() => _ScAcceptedCustomerAcceptedState();
}

class _ScAcceptedCustomerAcceptedState extends State<ScAcceptedCustomerAccepted> {
  // RefreshIndicator requires key to implement state (for RefreshIndicator)
  final GlobalKey<RefreshIndicatorState> _refreshKey = GlobalKey<RefreshIndicatorState>();

  List<QuoteData> availableAcceptedList;
  bool _isUpdating = false;

  //---------------------------------------------
  //---------------------------------------------
  void _setupAcceptedList() {
    availableAcceptedList = [];
    globalUserAcceptedList.forEach((element) {
      // remove any finished task from tblAccepted
      if (element.task.task_finished == 0) {
        availableAcceptedList.add(element);
      }
    });
  }

  //---------------------------------------------
  //---------------------------------------------
  Future<Null> _refresh() async {
    _isUpdating = true;

    await globalWebService.getUserTask(funcSuccess: () {
      _isUpdating = false;
      _setupAcceptedList();
    });

    setState(() => null);
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  void initState() {
    super.initState();
    print('[ScAcceptedCustomerAccepted]initState...');
  }

  //---------------------------------------------
  //---------------------------------------------
  void didChangeDependencies() async {
    super.didChangeDependencies();
    print('[ScAcceptedCustomerAccepted]didChangeDependencies...');

    _setupAcceptedList();
  }

  //---------------------------------------------
  //---------------------------------------------
  void dispose() {
    print("[ScAcceptedCustomerAccepted]dispose...");
    super.dispose();
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return Container(
      //margin: EdgeInsets.only(left: 10.0, right: 10.0),
      child: RefreshIndicator(
        key: _refreshKey,
        onRefresh: _refresh,
        child: ListView.builder(
          itemCount: availableAcceptedList.length,
          itemBuilder: (context, index) {
            return InfoCardAccepted(
              quoteData: availableAcceptedList[index],
              onTap: () async {
                if (_isUpdating) return;
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ScAcceptJobs(
                            acceptedQuote: availableAcceptedList[index],
                          )),
                );
                setState(() {
                  _setupAcceptedList();
                }); //maybe finished
              },
              onTapPhone: () async {
                if (_isUpdating) return;
                String url = 'tel:' + availableAcceptedList[index].seller.phone;

                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  await libShowErrorAlert(context, 'Error in calling the phone.');
                }
              },
              onTapEmail: () async {
                if (_isUpdating) return;
                String url = 'mailto:' + availableAcceptedList[index].seller.email;

                if (await canLaunch(url)) {
                await launch(url);
                } else {
                await libShowErrorAlert(context, 'Error in sending the mail.');
                }
              },
            );
          },
        ),
      ),
    );
  }
}
