import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:flutter/services.dart';

import '../global.dart';
import '../libMyLib.dart';
import '../libGetPicture.dart';
import '../objects/objEntryField.dart';
import '../objects/objAppButton.dart';

import 'scSuccess.dart';

//---------------------------------------------------------
//---------------------------------------------------------
class ScNewJob2 extends StatefulWidget {
  @override
  _ScNewJob2State createState() => _ScNewJob2State();
}

class _ScNewJob2State extends State<ScNewJob2> {
  String _inputTitle = '';
  String _inputDescription = '';
  String _imageBase64;

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  bool _isOperator = true;
  String _selectImageSource = '';
  File _image;

  bool _waiting = false;

  //---------------------------------------------
  //---------------------------------------------
  Future<void> _goSubmit(context) async {
    if (_inputTitle.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a title.');
      return;
    }
    if (_inputDescription.isEmpty) {
      await libShowErrorAlert(context, 'Please enter a description.');
      return;
    }

    globalNewJobData.title = _inputTitle;
    globalNewJobData.user_description = _inputDescription;

    setState(() {
      _waiting = true;
    });

    try {
      await globalWebService.createUserTask(
          imageBase64: _imageBase64,
          hire_type: _isOperator ? 'Wet Hire' : 'Dry Hire',
          funcSuccess: () async {
            // update user task list for the newly added task.
            await globalWebService.getUserTask();

            setState(() {
              _waiting = false;
            });

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ScSuccess()),
            );
          });
    } catch (e) {
      setState(() {
        _waiting = false;
      });
      String msg = e.toString();
      int idx = msg.indexOf(':') ?? -2;
      await libShowErrorAlert(context, msg.substring(idx + 2));
    }
  }

  //---------------------------------------------
  //---------------------------------------------
  Widget _bottomSheet(context) {
    return Container(
      child: Wrap(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.camera_alt),
              title: Text('Take a photo'),
              onTap: () {
                _selectImageSource = 'camera';
                Navigator.of(context).pop();
              }),
          ListTile(
            leading: Icon(Icons.image),
            title: Text('Image from gallery'),
            onTap: () {
              _selectImageSource = 'gallery';
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  //---------------------------------------------
  //---------------------------------------------
  // Get Image function
  //---------------------------------------------
  Future getImage() async {
    _selectImageSource = '';
    await showModalBottomSheet(context: context, builder: _bottomSheet);
    if (_selectImageSource.isEmpty) return;

    File savedImageFile =
        await getPicture(selectImageSource: _selectImageSource);

    List<int> imageBytes = savedImageFile.readAsBytesSync();
    _imageBase64 = base64Encode(imageBytes);

    print(_imageBase64.length);

    setState(() {
      _image = savedImageFile ?? null;
    });
  }

  @override
  //---------------------------------------------
  //---------------------------------------------
  Widget build(BuildContext context) {
    return ProgressHUD(
      inAsyncCall: _waiting,
      child: WillPopScope(
        onWillPop: () async => !_waiting,
        child: Scaffold(
          //-------------------
          //-- Title App Bar --
          //-------------------
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: GLOBAL_THEME_COLOR),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              'New job',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
          ),
          //--------------------------
          //-- body of entry fields --
          //--------------------------
          body: Container(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          //--------------------
                          //-- title field --
                          //--------------------
                          Row(
                            children: <Widget>[
                              Text(
                                'Title', // category previously
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.grey,
                                ),
                              ),
                              Text(
                                '*',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: GLOBAL_THEME_COLOR,
                                ),
                              ),
                            ],
                          ),
                          EntryField(
                            onChanged: (text) {
                              _inputTitle = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            controller: _titleController,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                          ),
                          SizedBox(height: 20),

                          //---------------------
                          //-- Picture content --
                          //---------------------
                          InkWell(
                            onTap: () => getImage(),
                            child: SizedBox(
                              width: 240,
                              height: 180,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    child: _image == null
                                        ? Container(
                                            width: 240,
                                            height: 180,
                                            color: Color(0x1fff0000),
                                            child: Center(
                                              child: Text(
                                                'Upload photo',
                                                style: TextStyle(
                                                  color: GLOBAL_THEME_COLOR,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                          )
                                        : Container(
                                            width: 240,
                                            height: 180,
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                fit: BoxFit.fill,
                                                image: FileImage(_image),
                                              ),
                                            ),
                                          ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Icon(
                                      Icons.add_photo_alternate,
                                      color: GLOBAL_THEME_COLOR,
                                      size: 50,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          //--------------
                          //-- Operator --
                          //--------------
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Operator',
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Row(
                            children: <Widget>[
                              //----------------
                              //-- Yes button --
                              //----------------
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _isOperator = true;
                                  });
                                },
                                child: Container(
                                  height: 40,
                                  width: 100,
                                  decoration: BoxDecoration(
                                    color: _isOperator
                                        ? GLOBAL_THEME_COLOR
                                        : Colors.white,
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        bottomLeft: Radius.circular(20)),
                                    border: Border.all(
                                        width: 1,
                                        color: GLOBAL_THEME_COLOR,
                                        style: BorderStyle.solid),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Yes, I need",
                                      style: TextStyle(
                                          color: _isOperator
                                              ? Colors.white
                                              : GLOBAL_THEME_COLOR,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                              //---------------
                              //-- No button --
                              //---------------
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _isOperator = false;
                                  });
                                },
                                child: Container(
                                  height: 40,
                                  width: 100,
                                  decoration: BoxDecoration(
                                    color: _isOperator
                                        ? Colors.white
                                        : GLOBAL_THEME_COLOR,
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(20),
                                        bottomRight: Radius.circular(20)),
                                    border: Border.all(
                                        width: 1,
                                        color: GLOBAL_THEME_COLOR,
                                        style: BorderStyle.solid),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "No, I don\'t",
                                      style: TextStyle(
                                          color: _isOperator
                                              ? GLOBAL_THEME_COLOR
                                              : Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          //-----------------------
                          //-- Description field --
                          //-----------------------
                          SizedBox(height: 20),
                          Row(
                            children: <Widget>[
                              Text(
                                'Description', // category previously
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.grey,
                                ),
                              ),
                              Text(
                                '*',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: GLOBAL_THEME_COLOR,
                                ),
                              ),
                            ],
                          ),
                          EntryField(
                            onChanged: (text) {
                              _inputDescription = text;
                            },
                            onSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            controller: _descriptionController,
                            keyboardType: TextInputType.multiline,
                            textInputAction: TextInputAction.newline,
                            maxLines: 4,
                          ),
                        ],
                      ),
                    ),
                  ),
                  //------------------
                  //-- Submit button --
                  //------------------
                  AppButton(
                    title: 'Submit',
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      await _goSubmit(context);
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }
}
