import 'package:flutter/material.dart';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_native_image/flutter_native_image.dart';

import 'global.dart';

Future<File> getPicture({
  @required String selectImageSource,
  bool isSquareImage = false,
}) async {
  //-- by image picker plugin
  File imageFile = await ImagePicker.pickImage(
    source: selectImageSource == 'camera'
        ? ImageSource.camera
        : ImageSource.gallery,
  );
  if (imageFile == null) return null;

  //-- by image cropper plugin
  File croppedImageFile = await ImageCropper.cropImage(
      sourcePath: imageFile.path,
      aspectRatio: isSquareImage
          ? CropAspectRatio(
              ratioX: 1,
              ratioY: 1,
            )
          : CropAspectRatio(
              ratioX: 4,
              ratioY: 3,
            ),
//      aspectRatioPresets: [
//        CropAspectRatioPreset.square,
//        CropAspectRatioPreset.ratio3x2,
//        CropAspectRatioPreset.original,
//        CropAspectRatioPreset.ratio4x3,
//        CropAspectRatioPreset.ratio16x9
//      ],
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: '',
        toolbarColor: GLOBAL_THEME_COLOR,
        toolbarWidgetColor: Colors.white,
        initAspectRatio: CropAspectRatioPreset.original,
        lockAspectRatio: true,
      ),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
        rotateButtonsHidden: true,
      ));
  if (croppedImageFile == null) return null;

  //-- resize file --
  File compressedFile = await FlutterNativeImage.compressImage(
    croppedImageFile.path,
    quality: 80,
    targetWidth: isSquareImage ? 384 : 512,
    targetHeight: 384,
  );

  // getting a directory path for saving
  imageCache.clear();
  Directory fileDirectory = await getApplicationDocumentsDirectory();
  File savedImageFile =
      await compressedFile.copy('${fileDirectory.path}/image.jpg');

  return savedImageFile;
}
