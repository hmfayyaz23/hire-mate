import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_map_location_picker/generated/i18n.dart'
    as location_picker;

import 'scenes/scSplash.dart';
import 'scenes/scLogin.dart';
import 'scenes/scMenu.dart';

//void main() => runApp(MyApp());
main() {
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // set only portrait mode (at services.dart)
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        // statusBarColor is used to set Status bar color in Android devices.
        statusBarColor: Colors.transparent,

        // To make Status bar icons color white in Android devices.
        //statusBarIconBrightness: Brightness.dark,

        // statusBarBrightness is used to set Status bar icon color in iOS.
        statusBarBrightness: Brightness.light
        // Here light means dark color Status bar icons.

        ));

    return MaterialApp(
      title: 'Hire Mate',
      //-- for google map location picker...
      localizationsDelegates: const [
        location_picker.S.delegate,
        //S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const <Locale>[
        Locale('en', ''),
      ],

      theme: ThemeData(
        // This is the theme of your application.
        //clea
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      //home: ScLogin(),
      routes: {
        //app routes
        'Splash': (context) => ScSplash(),
        'Login': (context) => ScLogin(),
        'Menu': (context) => ScMenu(),
      },
      initialRoute: 'Splash',
    );
  }
}
