import 'dart:convert';

import 'package:http/http.dart' as http;

import 'libMyLib.dart';

const String kGoogleMapKey = 'AIzaSyCrkjlpYYhjaMFq4i6CQrznyw7x8zmPFH0';

class LocationData {
  final bool isError;
  final double lat;
  final double lng;

  LocationData({this.isError, this.lat, this.lng});

  factory LocationData.fromJson(Map<String, dynamic> json) {
    return LocationData(
      isError: json['status'] != 'OK',
      lat: json['results'][0]['geometry']['location']['lat'],
      lng: json['results'][0]['geometry']['location']['lng'],
    );
  }
}

Future<LocationData> getLocation(String address) async {
  print('Start fetching data...');
  String url =
      'https://maps.googleapis.com/maps/api/geocode/json?key=$kGoogleMapKey&address=$address' +
          '&components=country:au';
  try {
    var response = await http.get(url);
    //prettyPrintJson(response.body.toString());
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      return LocationData.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      print('data error');
      return LocationData(isError: true);
      //throw Exception('Failed to load location data');
    }
  } catch (e) {
    print('Get Location Error');
    print(e);
    rethrow;
  }
}
